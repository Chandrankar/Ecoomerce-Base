import React, { useState } from 'react';
import { useRouter } from 'next/router';
import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined';
import SearchOutlinedIcon from '@mui/icons-material/SearchOutlined';
import PersonOutlineOutlinedIcon from '@mui/icons-material/PersonOutlineOutlined';
import SearchRoundedIcon from '@mui/icons-material/SearchRounded';
import FormatListBulletedRoundedIcon from '@mui/icons-material/FormatListBulletedRounded';
import DropdownButton from '../Dropdown/DropdownButton';
import Sidecart from '../Sidecart/Sidecart';
import CategorySidebar from '../Sidebar/categorySidebar';
import Link from 'next/link';
import { initFirebase } from '../../firebase/firebase.App';
import { getAuth } from 'firebase/auth';
import { useAuthState } from 'react-firebase-hooks/auth';

const Floatnav = () => {
  const { push } = useRouter();
  const [query, setQuery] = useState('');
  const [active, setActive] = useState(false);
  const auth = getAuth();
  const [user] = useAuthState(auth);

  const onSubmitHandler = (e) => {
    setActive(!active);
    e.preventDefault();
    push(`/search?query=${query}`);
  };

  const toggleIsActive = () => {
    setActive(!active);
  };

  const handleSearch = (e) => {
    setQuery(e.target.value);
  };

  return (
    <>
      <div className='bg-[#7A0A03] fixed bottom-0 flex items-center justify-between h-24 w-full text-[#F6DE8D] md:hidden z-30 duration-300 px-8'>
        <div>
          <Link href='/' className=' active:bg-red-400 rounded-full relative'>
            <HomeOutlinedIcon />
          </Link>
        </div>
        <div>
          <button
            className='rounded-full active:bg-red-400'
            onClick={toggleIsActive}
          >
            <SearchOutlinedIcon />
          </button>
        </div>
        <div>
          <CategorySidebar />
        </div>
        <div>
          <Sidecart className='ml-4' />
        </div>
       {user? <div>
          <DropdownButton Icon={PersonOutlineOutlinedIcon} />
        </div> : <div>
            <Link href='/login'>
              <PersonOutlineOutlinedIcon/>
            </Link>
          </div>}
      </div>
      <div
        className={`${
          active ? 'fixed' : 'hidden'
        } md:hidden p-4 pt-6 left-0 right-0 flex flex-col overflow-hidden z-20 h-full w-full`}
        style={{
          backgroundColor: 'rgb(30, 41, 59,0.7)',
        }}
      >
        <div className='bg-blue-100 shadow-xl rounded overflow-hidden'>
          <div className=' bg-gray-100  w-full'>
            <form onSubmit={onSubmitHandler} className='flex justify-between'>
              <button className='px-1  bg-white'>
                <SearchRoundedIcon />
              </button>
              <input
                type='text'
                placeholder='Search Sarees, Kurtis and more...'
                onChange={handleSearch}
                className='border-none bg-white w-full'
              />
              <button className='px-1 bg-white'>
                <FormatListBulletedRoundedIcon />
              </button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export default Floatnav;
