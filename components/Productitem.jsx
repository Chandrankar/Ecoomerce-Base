import React, {  useContext,useMemo,useState  } from 'react';
import {  useRouter  } from 'next/router';
import { Store } from '../utils/Store';
import { toast } from 'react-toastify';
import axios from 'axios';

const Productitem = ({product}) => {

  const{state,dispatch}=useContext(Store);

  

  const handleAddToCard = async () => {
    const existItem = state.cart.cartItems.find((x) => x.slug === product.slug);
    const quantity = existItem
      ? existItem.quantity + product.batchSize
      : product.batchSize;
    const { data } = await axios.get(`/api/products/${product._id}`);

    if (data.countInStock < quantity) {
      return toast.error('Product out of stock');
    }
    dispatch({ type: 'CART_ADD_ITEM', payload: { ...product, quantity } });
    toast.success('Product added to the cart');
  };

  const { push } = useRouter();
  return (
    <div className='bg-white max-sm:h-[600px] max-sm:w-full sm:h-[500px] sm:w-3/4 md:w-full min-[850px]:w-[300px] md:mr-4 md:mb-4'>
      <div className='h-full'>
        <div
          key={product.id}
          className='group relative h-3/5'
          onClick={() => push(`/product/${product.slug}`)}
        >
          <div className='h-full' style={{ width: '100%' }}>
            <img
              src={product.image}
              alt={product.name}
              style={{ height: '100%', width: '100%', objectFit: 'cover' }}
            />
          </div>
        </div>
        <div className='mt-4 flex flex-col items-left'>
          <div>
            <h3 className='text-sm text-gray-700'>
              <span aria-hidden='true' className='absolute inset-0' />
              {product.name}
            </h3>
            <p className='mt-1 text-sm text-gray-500'>{product.price}</p>
          </div>
          <button
            type='button'
            onClick={handleAddToCard}
            className='mt-4 text-red-700 w-fit'
          >
            Buy Now
          </button>
        </div>
      </div>
    </div>
  );
};
export default Productitem;
