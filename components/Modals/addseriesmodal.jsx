import React, { useState, useEffect, Fragment, useRef } from 'react';
import { useForm } from 'react-hook-form';
import axios from 'axios';
import { Dialog, Transition } from '@headlessui/react';
import { toast } from 'react-toastify';

const Addseriesmodal = () => {
  const { handleSubmit } = useForm();
  const [serieses, setSerieses] = useState([])
  const[series,setSeries] = useState('')


  useEffect(() => {
    async function getSerieses(){
      const result = await axios.get('/api/Series/getSeries')
      console.log(result.data)
      setSerieses(result.data)
    } getSerieses();
  }, [])
  
 
  const onSubmitHandler = async () => {
    console.log('called');
    try {
      const result = await axios.post('/api/Series/addSeries', {
        name,
      });
      toast.success('Series Added');
      setOpen(false);
    } catch (error) {
      toast.error('Cannot Add Series');
    }
  };
  const [open, setOpen] = useState(false);
  const [name, setName] = useState('');
  const cancelButtonRef = useRef(null);

  const deleteSeries =async()=>{
    try {const result = await axios.post('/api/Series/deleteSeries',{
      id: series
    })
    toast.success('Series Deleted')
  }catch(error){
      toast.error('Cannot Delete Series')
    }
  }

  return (
    <>
      <button className='primary-button' onClick={() => setOpen(true)}>
        Add Series
      </button>
      <Transition.Root show={open} as={Fragment}>
        <Dialog
          as='div'
          className='relative z-10'
          initialFocus={cancelButtonRef}
          onClose={setOpen}
        >
          <Transition.Child
            as={Fragment}
            enter='ease-out duration-300'
            enterFrom='opacity-0'
            enterTo='opacity-100'
            leave='ease-in duration-200'
            leaveFrom='opacity-100'
            leaveTo='opacity-0'
          >
            <div className='fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity' />
          </Transition.Child>

          <div className='fixed inset-0 z-10 overflow-y-auto'>
            <div className='flex min-h-full items-center justify-center p-4 text-center sm:p-0'>
              <Transition.Child
                as={Fragment}
                enter='ease-out duration-300'
                enterFrom='opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95'
                enterTo='opacity-100 translate-y-0 sm:scale-100'
                leave='ease-in duration-200'
                leaveFrom='opacity-100 translate-y-0 sm:scale-100'
                leaveTo='opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95'
              >
                <Dialog.Panel className='relative transform overflow-hidden rounded-lg bg-white text-left shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-lg'>
                  <div className='bg-white px-4 pb-4 pt-5 sm:p-6 sm:pb-4'>
                    <div className='sm:flex'>
                      <div className='w-full flex flex-col items-center'>
                        <Dialog.Title
                          as='h3'
                          className='text-base font-semibold leading-6 text-gray-900'
                        >
                          Manage Broker
                        </Dialog.Title>
                        <div>
                          <form
                            onSubmit={handleSubmit()}
                            className='mt-10 pt-4 '
                          >
                            <div className='flex justify-between my-4'>
                              <label className='pt-1'>Select Series</label>
                              <div className='flex'>
                              <select
                                  value={series}
                                  onChange={(e) =>
                                    setSeries(e.target.value)
                                  }
                                  className='rounded-md mx-2'
                                >
                                  <option key="EmptySeries" value=""></option>
                                  {serieses.map((bro,index)=>(
                                    <option key={bro._id} value={bro._id}>{bro.Series_Name}</option>
                                  ))}
                                </select>
                                <button onClick={deleteSeries} className='inline-flex w-full justify-center rounded-md bg-red-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-red-500 sm:ml-3 sm:w-auto mx-2'>
                                Delete Series
                              </button>
                              </div>
                            </div>
                            <div className='flex justify-between my-4'>
                              <label className='pt-1'>Or Add New Series</label>
                            </div>
                            <div className='flex justify-between'>
                              <label className='pt-1'>Series Name</label>
                              <div className='flex'>
                                <input
                                  type='text'
                                  className='rounded-md mx-2'
                                  onChange={(e) => setName(e.target.value)}
                                />
                              </div>
                            </div>
                            <div className='px-4 py-3 sm:flex sm:flex-row-reverse sm:px-6'>
                              <button
                                type='button'
                                className='mt-3 inline-flex w-full justify-center rounded-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50 sm:mt-0 sm:w-auto'
                                onClick={() => setOpen(false)}
                              >
                                Cancel
                              </button>
                              <button className='inline-flex w-full justify-center rounded-md bg-red-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-red-500 sm:ml-3 sm:w-auto mx-2' onClick={()=>onSubmitHandler}>
                                Add Series
                              </button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition.Root>
    </>
  );
};

export default Addseriesmodal;