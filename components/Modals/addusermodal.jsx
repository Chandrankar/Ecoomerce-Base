import React, { useState,  Fragment, useRef,useMemo } from 'react';
import { useForm } from 'react-hook-form';
import axios from 'axios';
import { Dialog, Transition } from '@headlessui/react';
import { toast } from 'react-toastify';
import PhoneInput from 'react-phone-input-2';
import { initFirebase } from '../../firebase/firebase.App';
import { getAuth } from 'firebase/auth';
import { useAuthState } from 'react-firebase-hooks/auth';

const Addusermodal = ({ org_id, org_name }) => {
  const { handleSubmit } = useForm();
  const submitHandler = async () => {
    try {
      const result = await axios.post('/api/addNewUser', {
        name,
        mobile_number: ph,
        organization: org_id,
        allowed_sign_in: true,
        role: role,
      });
      toast.success('Organization Added');
      setOpen(false);
    } catch (error) {
      toast.error('Cannot add Organization');
    }
  };

  const [open, setOpen] = useState(false);
  const [name, setName] = useState('');
  const cancelButtonRef = useRef(null);
  const [ph, setPh] = useState('');
  const [role, setRole] = useState('');
  const auth = getAuth();
  const [user] = useAuthState(auth);
  const[userdet, setUserdet]= useState({})

  async function getUser(){
    try{
      var phone_number = user.phoneNumber
      var ph = phone_number.slice(1)
      const result = await axios.post('/api/getUserDetails',{mobile_number: ph})
      setUserdet(result.data)
    }catch(error){
      console.log(error)
    }
  }

  useMemo(() => getUser(), [user])

  return (
    <>
      <button className='primary-button mb-4' onClick={() => setOpen(true)}>
        Add User
      </button>
      <Transition.Root show={open} as={Fragment}>
        <Dialog
          as='div'
          className='relative z-10'
          initialFocus={cancelButtonRef}
          onClose={setOpen}
        >
          <Transition.Child
            as={Fragment}
            enter='ease-out duration-300'
            enterFrom='opacity-0'
            enterTo='opacity-100'
            leave='ease-in duration-200'
            leaveFrom='opacity-100'
            leaveTo='opacity-0'
          >
            <div className='fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity' />
          </Transition.Child>

          <div className='fixed inset-0 z-10 overflow-y-auto'>
            <div className='flex min-h-full items-end justify-center p-4 text-center sm:items-center sm:p-0'>
              <Transition.Child
                as={Fragment}
                enter='ease-out duration-300'
                enterFrom='opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95'
                enterTo='opacity-100 translate-y-0 sm:scale-100'
                leave='ease-in duration-200'
                leaveFrom='opacity-100 translate-y-0 sm:scale-100'
                leaveTo='opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95'
              >
                <Dialog.Panel className='relative transform overflow-hidden rounded-lg bg-white text-left shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-lg'>
                  <div className='bg-white px-4 pb-4 pt-5 sm:p-6 sm:pb-4'>
                    <div className='sm:flex sm:items-start'>
                      <div className='mt-3 text-center sm:ml-4 sm:mt-0 sm:text-left'>
                        <Dialog.Title
                          as='h3'
                          className='text-base font-semibold leading-6 text-gray-900'
                        >
                          {`Add new user to ${org_name}`}
                        </Dialog.Title>
                        <div className='mt-2'>
                          <form
                            onSubmit={handleSubmit(submitHandler)}
                            className='mt-10 pt-4 '
                          >
                            <div className='flex justify-between p-2'>
                              <label className='pt-1'>User Name</label>
                              <div className='flex'>
                                <input
                                  type='text'
                                  className='rounded-md mx-2'
                                  onChange={(e) => setName(e.target.value)}
                                />
                              </div>
                            </div>
                            <div className='flex justify-between p-2'>
                              <label className='pt-1'>Mobile Number</label>
                              <div className='flex'>
                                <PhoneInput
                                  country={'in'}
                                  value={ph}
                                  onChange={setPh}
                                />
                              </div>
                            </div>
                            <div className='flex justify-between p-2'>
                              <label className='pt-1'>Role</label>
                              <div className='flex'>
                                <select
                                  value={role}
                                  onChange={(e) => setRole(e.target.value)}
                                  className='rounded-md mx-2'
                                >
                                  <option value={''}>-----</option>
                                  <option value={'User'}>User</option>
                                  <option value={'Staff'}>Staff</option>
                                  {(userdet?.role=="Admin" || userdet.role=="Manager") && <option value={'Manager'}>Manager</option>}
                                  {userdet?.role=="Admin" && <option value={'Admin'}>Admin</option>}
                                </select>
                              </div>
                            </div>
                            <div className=' px-4 py-3 sm:flex sm:flex-row-reverse sm:px-6'>
                              <button
                                type='button'
                                className='mt-3 inline-flex w-full justify-center rounded-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50 sm:mt-0 sm:w-auto'
                                onClick={() => setOpen(false)}
                              >
                                Cancel
                              </button>
                              <button className='inline-flex w-full justify-center rounded-md bg-red-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-red-500 sm:ml-3 sm:w-auto mx-2'>
                                Add User
                              </button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition.Root>
    </>
  );
};

export default Addusermodal;
