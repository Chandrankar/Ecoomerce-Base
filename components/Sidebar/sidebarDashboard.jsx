import React, { useState,useMemo } from 'react';
import HomeIcon from '@mui/icons-material/Home';
import BarChartIcon from '@mui/icons-material/BarChart';
import PeopleAltIcon from '@mui/icons-material/PeopleAlt';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import DescriptionIcon from '@mui/icons-material/Description';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import Link from 'next/link';
import { initFirebase } from '../../firebase/firebase.App';
import { getAuth } from 'firebase/auth';
import { useAuthState } from 'react-firebase-hooks/auth';
import axios from 'axios';


const SidebarDashboard = () => {
  const [open, setOpen] = useState(false);
  const [isSubMenuOpen, setIsSubMenuOpen] = useState(false);
  const auth = getAuth();
  const [user] = useAuthState(auth);
  const[userdet, setUserdet]= useState({})

  const opensubmenu =(menu)=>{
    if(!menu.submenuItems){return}
    setIsSubMenuOpen(!isSubMenuOpen)
  }
  async function getUser(){
    try{
      var phone_number = user.phoneNumber
      var ph = phone_number.slice(1)
      const result = await axios.post('/api/getUserDetails',{mobile_number: ph})
      setUserdet(result.data)
    }catch(error){
      console.log(error)
    }
  }

  useMemo(() => getUser(), [user])

  const[perl, setPerl] = useState(0);
   function setPermissionLevel(){
    if(userdet.role =="Admin")(setPerl(4))
    if(userdet.role =="Manager")(setPerl(3))
    if(userdet.role =="Staff")(setPerl(2))
    if(userdet.role =="User")(setPerl(1))
    if(!user){return 0}
   }

   useMemo(() => setPermissionLevel(), [userdet])
  const Menus = [
    { title: 'Home', icon: <HomeIcon />, href: '/',permission:1 },
    {
      title: 'Organizations',
      icon: <BarChartIcon />,
      href: '/listOrganization',
      permission: 2,
    },
    { title: 'Users', icon: <PeopleAltIcon />, href: '/listUsers',permission: 4 },
    {
      title: 'E-commerce',
      icon: <ShoppingCartIcon />,
      submenu: true,
      permission: 4,
      submenuItems:[
        {title:"Products", href:"/listProducts"},
        {title:"Add Product", href: "/addproduct"},
        {title:"Orders", href:"/listorders"}
      ]
      },
      {title:"My Organization", href:`/Organization/${userdet.organization}`,icon:<DescriptionIcon/>,permission: 2},
      {title:"Refund/Cacelations", href:"/refunds",icon:<DescriptionIcon/>,permission: 4},
      {title:"My Orders", href:`/orderhistory/${userdet._id}`,icon:<DescriptionIcon/>,permission: 1},
      {title:"My Files", href:`/user/${userdet._id}`,icon:<DescriptionIcon/>,permission: 1},
      {title:"Specials", href:`/specials/${userdet._id}`,icon:<DescriptionIcon/>,permission: 1},
    ]

    
  return (
    <div
      className={`bg-red-primary h-screen fixed-insert-0 ${
        open ? 'w-72' : 'w-20'
      } p-5 pt-8 relative duration-300 h-auto flex flex-col items-center`}
    >
      <button
        className={`absolute right-[-10px] bg-gold-primary rounded-full w-[30px] h-[30px] flex justify-center items-center`}
        onClick={() => setOpen(!open)}
      >
        {open ? (
          <ArrowBackIosNewIcon sx={{ color: 'black', fontSize: '16px' }} />
        ) : (
          <ArrowForwardIosIcon sx={{ color: 'black', fontSize: '16px' }} />
        )}
      </button>

      <ul className='mt-10'>
        {Menus.map((menu, index) => (
          <>
            {menu.permission<= perl && <li
              onClick={() => opensubmenu(menu)}
              key={menu.href}
              className='text-white text-sm flex items-center gap-x-4 cursor-pointer p-2 hover:bg-red-500 rounded-md mt-2'
            >
              {!menu.submenu && (
                <Link className='flex items-center gap-x-4' href={menu.href}>
                  {menu.icon}
                  {open && <span>{menu.title}</span>}
                </Link>
              )}
              {menu.submenu && (
                <>
                  {menu.icon}
                  {open && <span>{menu.title}</span>}
                  {menu.submenu && open && (
                    <KeyboardArrowDownIcon
                      className={`${isSubMenuOpen && 'rotate-180'}`}
                    />
                  )}
                </>
              )}
            </li>}
            {menu.submenu && isSubMenuOpen && open && (
              <ul>
                {menu.submenuItems.map((submenuItem) => (
                  <li
                    key={submenuItem.title}
                    className='text-white text-sm flex items-center gap-x-4 cursor-pointer p-2 px-5 hover:bg-red-500 rounded-md mt-2 duration-300'
                  >
                    <Link href={submenuItem.href}>{submenuItem.title}</Link>
                  </li>
                ))}
              </ul>
            )}
          </>
        ))}
      </ul>
    </div>
  );
};

export default SidebarDashboard;
