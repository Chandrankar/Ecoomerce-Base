import React from 'react'
import {ref,getDownloadURL} from 'firebase/storage'
import {storage} from '../../firebase/firebase.App'
import { useRouter } from 'next/router' 

const Downloadfile = (name) => {

    const downloadFB =()=>{
        const pathRef = ref(storage,`gs://gauripuja-186c5.appspot.com/${name.name}`)
        getDownloadURL(pathRef).then((url)=>{
          const newWindow = window.open(url, '_blank', 'noopener,noreferrer')
          if (newWindow) newWindow.opener = null;
    })}
  return (
    <div>
        <button onClick={downloadFB} className="text-black">Download</button>
    </div>
    
  )
}

export default Downloadfile