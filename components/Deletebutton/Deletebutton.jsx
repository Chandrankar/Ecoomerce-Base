import React from 'react'
import { ref, deleteObject } from "firebase/storage";
import {toast} from 'react-toastify'
import {storage} from '../../firebase/firebase.App'


const Deletebutton = ({fullPath}) => {
    const desertRef = ref(storage, `${fullPath}`);
    function deleteFile(){
        deleteObject(desertRef).then(() => {
            toast.success("Item Deleted");
          }).catch((error) => {
            toast.error("Cannot Delete")
          });
    }
    
      return(
        <button onClick={deleteFile}>Delete</button>
      )
}

export default Deletebutton