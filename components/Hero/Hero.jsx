import React, { useState, Fragment, useMemo } from 'react';
import Image from 'next/image';
import SearchRoundedIcon from '@mui/icons-material/SearchRounded';
import FormatListBulletedRoundedIcon from '@mui/icons-material/FormatListBulletedRounded';
import PermIdentitySharpIcon from '@mui/icons-material/PermIdentitySharp';
import Sidecart from '../Sidecart/Sidecart';
import Dropdown from '../Dropdown/Dropdown';
import { useRouter } from 'next/router';
import { Disclosure, Menu, Transition } from '@headlessui/react';
import Link from 'next/link';
import { initFirebase } from '../../firebase/firebase.App';
import { getAuth } from 'firebase/auth';
import { useAuthState } from 'react-firebase-hooks/auth';
import axios from 'axios';
import useEffectOnce from '../../Hooks/useEffectOnce';

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

const Hero = () => {
  const [categories, setCategories] = useState([]);

  useEffectOnce(() => {
    async function getcategories() {
      try {
        const cat = await axios.get('/api/getCategories');
        //console.log(cat.data)
        setCategories(cat.data);
        //categories = cat.data
      } catch (error) {
        toast.error('something went wrong');
      }
    }
    getcategories();
  }, []);
  const app = initFirebase();
  const { push } = useRouter();
  const [query, setQuery] = useState('');
  const submitHandler = (e) => {
    console.log('Clicked');
    e.preventDefault();
    push(`/search?query=${query}`);
  };
  const auth = getAuth();
  const [user] = useAuthState(auth);
  const[userdet, setUserdet]= useState({})

  async function getUser(){
    try{
      var phone_number = user.phoneNumber
      var ph = phone_number.slice(1)
      const result = await axios.post('/api/getUserDetails',{mobile_number: ph})
      setUserdet(result.data)
    }catch(error){
      console.log(error)
    }
  }

  useMemo(() => getUser(), [user])

  const isUserLoggedIn = user ? true : false;

  return (
    <div className='bg-gradient-to-t from-red-500 to-orange-500 pt-4 w-full md:z-30'>
      <div className='flex justify-between mx-4'>
        <Image
          src='/favicon.ico'
          alt='me'
          width='40'
          height='64'
          style={{
            objectFit: 'contain',
          }}
        />
        <div className='hidden md:block bg-gray-100 rounded-lg w-1/2'>
          <form onSubmit={submitHandler} className='flex justify-between'>
            <button className='px-1'>
              <SearchRoundedIcon />
            </button>
            <input
              type='text'
              placeholder='Search Sarees, Kurtis and more...'
              onChange={(e) => setQuery(e.target.value)}
              className='border-none bg-gray-100 w-full rounded-r-md'
            />
          </form>
        </div>
        <div className='hidden md:flex px-4 text-white text-xl text-center items-center'>
          {isUserLoggedIn ? (
            <Menu as='div' className='relative ml-3'>
              <div>
                <Menu.Button className='flex rounded-full bg-gray-800 text-sm focus:outline-none focus:ring-2 focus:ring-white focus:ring-offset-2 focus:ring-offset-gray-800'>
                  <span className='sr-only'>Open user menu</span>
                  <img
                    className='h-8 w-8 rounded-full'
                    src='https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80'
                    alt=''
                  />
                </Menu.Button>
              </div>
              <Transition
                as={Fragment}
                enter='transition ease-out duration-100'
                enterFrom='transform opacity-0 scale-95'
                enterTo='transform opacity-100 scale-100'
                leave='transition ease-in duration-75'
                leaveFrom='transform opacity-100 scale-100'
                leaveTo='transform opacity-0 scale-95'
              >
                <Menu.Items className='absolute right-0 z-10 mt-2 w-48 origin-top-right rounded-md bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none'>
                  <Menu.Item key="specials">
                    {({ active }) => (
                      <Link
                        href={`/specials/${userdet._id}`}
                        className={classNames(
                          active ? 'bg-gray-100' : '',
                          'block px-4 py-2 text-sm text-gray-700'
                        )}
                      >
                        Specials
                      </Link>
                    )}
                  </Menu.Item>
                  <Menu.Item key="Dashboard">
                    {({ active }) => (
                      <Link
                        href='/dashboard'
                        className={classNames(
                          active ? 'bg-gray-100' : '',
                          'block px-4 py-2 text-sm text-gray-700'
                        )}
                      >
                        Dashboard
                      </Link>
                    )}
                  </Menu.Item>
                  <Menu.Item key="Signout">
                    {({ active }) => (
                      <Link
                        onClick={() => auth.signOut()}
                        href='/'
                        className={classNames(
                          active ? 'bg-gray-100' : '',
                          'block px-4 py-2 text-sm text-gray-700'
                        )}
                      >
                        Signout
                      </Link>
                    )}
                  </Menu.Item>
                </Menu.Items>
              </Transition>
            </Menu>
          ) : (
            <div
              className='flex mx-2 text-center items-center cursor-pointer'
              onClick={() => push('/login')}
            >
              <PermIdentitySharpIcon sx={{ fontSize: '20px' }} />
              <p className='text-base ml-1'>Sign In</p>
            </div>
          )}

          <div className='mx-2 text-center items-end'>
            <Sidecart />
          </div>
        </div>
      </div>
      {/* TODO: Make this Responsive */}
      <div className='hidden md:flex justify-between text-white px-4 pt-4'>
        <button>Home</button>
        {categories.map((cat,index) => (
          <div key={index}>
            <Dropdown key={cat.name} name={cat.name} />
          </div>
        ))}
        <Link href='/dashboard'>Dashboard</Link>
      </div>
      <div className='flex justify-between px-8'>
        <div className='pt-24 pl-12 mb-4'>
          <div className='p-4'>
            <p className='text-white text-2xl py-2'>
              Best Deal Online for Ethnic
            </p>
            <p className='text-amber-200 text-6xl font-bold py-2'>
              Wear The Culture
            </p>
            <p className='text-white text-2xl'>UP to 80% OFF</p>
          </div>
          <div className='px-4'>
            <button
              className='text-red-700 bg-amber-300 rounded-md p-2 hover:bg-amber-200 active:bg-amber-400 focus:ring-2         '
              onClick={() => push('/about')}
            >
              Learn More
            </button>
          </div>
        </div>
        <div className='hidden md:block px-8'>
          <Image src='/hero.png' alt='Picture' width={350} height={200} />
        </div>
      </div>
      <div className='md:hidden'>
        <Image src='/hero.png' alt='Picture' width={150} height={150} />
      </div>
    </div>
  );
};

export default Hero;
