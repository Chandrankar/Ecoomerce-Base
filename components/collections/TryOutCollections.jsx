import React from 'react';
import Image from 'next/image';

const Item = ({ src, alt, onClick }) => {
  return (
    <div
      className='flex flex-col items-center cursor-pointer mt-6'
      onClick={onClick}
    >
      <div className='flex jusitfy-center mr-4 relative w-[120px] h-[120px]  md:max-lg:w-[180px] md:max-lg:h-[180px] lg:w-[200px] lg:h-[200px] rounded-md outline-1 overflow-hidden outline-slate-300'>
        <Image src={src} alt={alt} fill style={{ objectFit: 'cover' }} />
      </div>
      <p className='mt-4'>Kurta 1</p>
    </div>
  );
};

const TryOutCollections = () => {
  const handleItemClick = () => {
    //Add Handling Logic here.
  };

  return (
    <div className='px-8 flex flex-col items-center'>
      <div className='text-center mb-4'>
        <p className='text-red-primary text-4xl font-regular'>
          Try Out For Wedding Collection
        </p>
      </div>
      <div className='text-black w-1/2'>
        <p className='text-center'>
          {`Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text.`}
        </p>
      </div>
      <div className='mt-6 flex flex-row flex-wrap justify-center'>
        <Item src='/Saree1.png' alt='Saree1' onClick={handleItemClick} />
        <Item src='/Saree2.png' alt='Saree2' onClick={handleItemClick} />
        <Item src='/Saree3.png' alt='Saree3' onClick={handleItemClick} />
        <Item src='/Man1.png' alt='Kurta1' onClick={handleItemClick} />
      </div>
    </div>
  );
};

export default TryOutCollections;
