import React from 'react';
import Image from 'next/image';

const Collection = () => {
  return (
    <div className='p-8 flex flex-col items-center'>
      <div className='text-center mb-10'>
        <p className='text-red-primary text-4xl font-regular'>
          Try Out Summer Collection
        </p>
      </div>

      <div className='text-black w-full sm:w-1/2'>
        <p className='text-center'>
          Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dolor non in
          autem cum porro quidem qui nostrum. Aperiam laboriosam expedita
          debitis, maiores amet facere cum magni voluptate odit nulla
          cupiditate.
        </p>
      </div>

      <div className='flex flex-col md:flex-row justify-center items-center p-4 m-4 w-full max-w-[800px]'>
        {/* Image 1 */}
        <div className='relative rounded-md cursor-pointer w-full sm:w-2/3 md:w-1/3 h-[400px]'>
          <p className='text-[#7A0A03] text-base font-medium bg-white text rounded absolute inset-x-0 bottom-0 m-2 py-3 text-center z-10'>
            Best Of the Month
          </p>
          <Image
            src='/Saree1.png'
            alt='Saree1'
            fill
            style={{ objectFit: 'cover' }}
          />
        </div>

        {/* Image 2 */}
        <div className='relative rounded-md m-2 cursor-pointer w-full sm:w-2/3 md:w-1/3 h-[400px]'>
          <p className='text-[#7A0A03] text-base font-medium bg-white text rounded absolute inset-x-0 bottom-0 m-2 py-3 text-center z-10'>
            New Arrival
          </p>

          <Image
            src={'/Saree2.png'}
            alt={'Saree2'}
            fill
            style={{ objectFit: 'cover' }}
          />
        </div>

        {/* Image 3 & 4 */}
        <div className='flex flex-col md:w-1/3 md:max-h-[400px] w-full items-center'>
          <div className='relative rounded-md w-full sm:w-2/3 md:w-full h-[400px] mb-2 cursor-pointer'>
            <p className='text-[#7A0A03] text-base font-medium bg-white text rounded absolute inset-x-0 bottom-0 m-2 py-3 text-center z-10'>
              Classic One
            </p>
            <Image
              src='/Man1.png'
              alt='Man1'
              fill
              style={{ objectFit: 'cover' }}
            />
          </div>
          <div className='relative rounded-md w-full sm:w-2/3 md:w-full h-[400px] cursor-pointer'>
            <p className='text-[#7A0A03] text-base font-medium bg-white text rounded absolute inset-x-0 bottom-0 m-2 py-3 text-center z-10'>
              Try Different
            </p>
            <Image
              src='/Saree3.png'
              alt='Saree3'
              fill
              style={{ objectFit: 'cover' }}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Collection;
