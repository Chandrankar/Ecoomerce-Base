import React, {useContext} from 'react';
import {useRouter} from 'next/router';
import { Store } from '../utils/Store';
import { toast } from 'react-toastify';
import axios from 'axios';
import { initFirebase } from '../firebase/firebase.App';
import { getAuth } from 'firebase/auth';
import { useAuthState } from 'react-firebase-hooks/auth';
import { useMemo } from 'react';
import { useState } from 'react';


const Productitem = (special) => {
    const product = special.product.product;
    const specials = special;
    console.log("specials",special)
  const{state,dispatch}=useContext(Store);

  const AddtocartHandler =async()=>{
    const existItem = state.cart.cartItems.find((x)=>x.slug === product.slug);
    const quantity = existItem? existItem.quantity + product.batchSize: product.batchSize
    const {data} = await axios.get(`/api/products/${product.Proid}`);
  
    if(data.countInStock < quantity){
        return toast.error('Product out of stock');
    }
    dispatch({type: 'CART_ADD_ITEM',payload:{...product,quantity}});
    toast.success('Product added to the cart');
  };

  const removeSpecial =async()=>{
    //console.log(specials.product)
    const del = await axios.post('/api/deletespl',{id: specials.product._id})
  } 
  const {push} = useRouter();
  const app = initFirebase();
  const auth = getAuth();
  const [user] = useAuthState(auth);
  const [userdet,setUserdet] = useState({})

  async function getUser(){
    try{
      var phone_number = user.phoneNumber
      var ph = phone_number.slice(1)
      const result = await axios.post('/api/getUserDetails',{mobile_number: ph})
      setUserdet(result.data)
      console.log(result.data)
    }catch(error){
      console.log(error)
    }
  }

  useMemo(() => getUser(), [user])
  return (
    <div className="bg-white">
      <div className="mx-auto max-w-2xl py-4 px-4 sm:py-4 sm:px-6 lg:max-w-7xl lg:px-4">
            <div key={product.id} className="group relative" onClick={()=>push(`/product/${product.slug}`)}>
              <div className="min-h-80 aspect-w-1 aspect-h-1 w-full overflow-hidden rounded-md bg-gray-200 group-hover:opacity-75 lg:aspect-none lg:h-80" >
                <img
                  src={product.image}
                  alt={product.name}
                  className="h-full w-full object-cover object-center lg:h-full lg:w-full"
                />
              </div>
              <div className="mt-4 flex justify-between">
                <div>
                  <h3 className="text-sm text-gray-700">
                      <span aria-hidden="true" className="absolute inset-0" />
                      {product.name}
                  </h3>
                  <p className="mt-1 text-sm text-gray-500">{product.price}</p>
                  
                </div>
              </div>
            </div>
            <div className="flex justify-between">
                  <div></div>
                  {!userdet.role =="Admin"? <button type="button" onClick={AddtocartHandler} className="ml-8 text-red-700">Buy Now</button>
                  :<button type="button" onClick={removeSpecial} className="ml-8 text-red-700">Remove</button>}
                </div>
        </div>
      </div>
  )
}

export default Productitem