/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: false,
  publicRuntimeConfig:{
    BACKEND_URL: process.env.BACKEND_URL
  }
};
