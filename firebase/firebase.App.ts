import { initializeApp } from "firebase/app";
import {getAuth} from 'firebase/auth'
import {getStorage} from "firebase/storage"
import {getFirestore} from "firebase/firestore"
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCh0JA6LDSGNgrzEIrb2CfIJlrMMHHIxi4",
  authDomain: "gauripujahosting-c213d.firebaseapp.com",
  projectId: "gauripujahosting-c213d",
  storageBucket: "gauripujahosting-c213d.appspot.com",
  messagingSenderId: "216186251089",
  appId: "1:216186251089:web:bfbfd33c25cb6500ac1570",
  measurementId: "G-28HYNH6H0G"
};


// Initialize Firebase
export const app = initializeApp(firebaseConfig);

export const initFirebase=()=>{
  return app
}

const db = getFirestore()
export {db}

export const auth = getAuth(app)
export const storage = getStorage(app)