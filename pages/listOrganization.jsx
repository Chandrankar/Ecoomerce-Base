import React, { useState } from 'react';
import Layout from '../components/Layout/Layout';
import SidebarDashboard from '../components/Sidebar/sidebarDashboard';
import { DataGrid } from '@mui/x-data-grid';
import axios from 'axios';
import { useRouter } from 'next/router';
import { toast } from 'react-toastify';
import db from '../utils/db';
import Organization from '../models/Organization';
import Addorgmodal from '../components/Modals/addorgmodal';
import { useMemo } from 'react';
import { initFirebase } from '../firebase/firebase.App';
import { getAuth } from 'firebase/auth';
import { useAuthState } from 'react-firebase-hooks/auth';

const ListUsers = (props) => {
  const { push } = useRouter();
  const[userdet, setUserdet]= useState({})
  const auth = getAuth();
  const [user] = useAuthState(auth);
  async function getUser(){
    try{
      var phone_number = user.phoneNumber
      var ph = phone_number.slice(1)
      const result = await axios.post('/api/getUserDetails',{mobile_number: ph})
      setUserdet(result.data)
    }catch(error){
      console.log(error)
    }
  }

  useMemo(() => getUser(), [user])


  const columns = [
    { field: '_id', headerName: 'Organization Id', width: 300 },
    { field: 'name', headerName: 'Name', width: 100 },
    {
      field: 'details',
      headername: 'Show Details',
      width: 100,
      renderCell: ({ row: { _id } }) => {
        return (
          <div>
            <button onClick={() => push(`/Organization/${_id}`)}>
              Show Details
            </button>
          </div>
        );
      },
    },
  ];

  return (
    <Layout>
      <div className='flex'>
        <SidebarDashboard />
        <div className='mx-4 my-4 w-full overflow-x-scroll'>
          <div className='mb-4'>
            {userdet.role=="Admin" && <Addorgmodal className='m-4' />}
          </div>
          <DataGrid
            columns={columns}
            rows={props.orgs}
            getRowId={(org) => org._id}
          />
        </div>
      </div>
    </Layout>
  );
};

export async function getServerSideProps() {
  await db.connect();
  const orgs = await Organization.find().limit().lean();
  await db.disconnect();
  return {
    props: {
      orgs: orgs.map(db.convertDocToObj),
    },
  };
}

export default ListUsers;
