import React, { useState } from 'react';
import Layout from '../components/Layout/Layout';
import {useForm} from 'react-hook-form';
import SearchIcon from '@mui/icons-material/Search';
import { useRouter } from 'next/router';
import axios from 'axios'
import {DataGrid, GridToolbar} from '@mui/x-data-grid';

const Ordersearch = () => {
    const {push} = useRouter();
    const [phone,setPhone] = useState('');
    const [orderid, setOrderid] = useState('')
    const [orders, setOrders] = useState();
    const{handleSubmit} = useForm();
    const formSubmit =async ()=>{
       const result =await  axios.post('/api/getOrderPh',{phone: phone})
        console.log(result.data)
        setOrders(result.data)
    }
    const columns=[
        {field:"_id",headerName:"Order Id",flex:1},
        {field:"totalPrice",headerName:"Total Price",flex:1},
        {field:"isPaid",headerName:"Payment Status", flex:1},
        {field:"isDelivered",headerName:"Delivery Status",flex:1},
        {field:"status",headerName:"Status",flex:1},
        {field:"createdAt",headerName:"Order Date",flex:1},
        {field:"details", headername:"Show Details",flex:1,renderCell:({row:{_id}})=>{
          return(
              <div>
                  <button onClick={()=>push(`/order/${_id}`)}>Show Details</button>
              </div>
          )
      }}
    ]
  return (
    <Layout>
        <div className="flex mx-4 mt-4">
            <form onSubmit={handleSubmit(formSubmit)} className="flex">
                <label htmlFor="orderId" className="p-4">Enter Your Phone Number</label>
                <div className="border-2 rounded-md">
                    <input type="text" className="border-0 p-4" onChange={(e)=>setPhone(e.target.value)}/>
                    <button className="mt-2"><SearchIcon/></button>
                </div>
            </form>
        </div>

        <div className='mx-4 my-4 w-full h-screen overflow-x-scroll'>
        {orders && <DataGrid
          columns={columns}
          rows={orders}
          getRowId={(ord) => ord._id}
        />}
      </div>
    </Layout>
  )
}

export default Ordersearch