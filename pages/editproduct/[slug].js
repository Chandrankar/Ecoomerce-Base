import React, { useContext, useState } from 'react';
import Layout from '../../components/Layout/Layout';
import { useRouter } from 'next/router';
import Link from 'next/link';
import Image from 'next/image';
import { Store } from '../../utils/Store';
import db from '../../utils/db';
import Product from '../../models/Product';
import SidebarDashboard from '../../components/Sidebar/sidebarDashboard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import ManageCatagories from '../../components/manageCategories';
import { useEffect } from 'react';

export default function ProductScreen(props) {
  const { product } = props;
  const { push } = useRouter();
  const { handleSubmit, setValue } = useForm();

  const [name, setName] = useState(product.name);
  const [category, setCategory] = useState(product.category);
  const [subcategory, setSubCategory] = useState(product.subCategory);
  const [isPublic, setIsPublic] = useState(product.isPublic);
  const [stock, setStock] = useState(product.countInStock);
  const [batch, setBatch] = useState(product.batchSize);
  const [desc, setDesc] = useState(product.description);
  const [price, setPrice] = useState(product.price);
  const [error, setError] = useState('');
  const [categories, setCategories] = useState([]);
  const [subCat, setSubCat] = useState([]);
  const [file, setFile] = useState();
  const [imageUrl, setImageUrl] = useState(product.image);

  const id = product._id;

  async function getSubCat(name) {
    setCategory(name);
    if (name === '') return;
    const sub = await axios.post('/api/getSubCat', { name });
    console.log(sub.data);
    setSubCat(sub.data);
  }

  useEffect(() => {
    async function getcategories() {
      try {
        const cat = await axios.get('/api/getCategories');
        setCategories(cat.data);
      } catch (error) {
        toast.error('something went wrong');
      }
    }
    getcategories();
  }, [name]);

  async function submitHandler() {
    if (file) {
      const { url } = await fetch('/api/imageUpload').then((res) => res.json());
      await fetch(url, {
        method: 'PUT',
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        body: file,
      });
      const ImageUrl = await url.split('?')[0];
      setImageUrl(ImageUrl);
    }
    const slug = product.slug;

    try {
      axios.post('/api/updateproductmongo', {
        id,
        name,
        category,
        subcategory,
        slug,
        price,
        batch,
        stock,
        desc,
        imageUrl,
        isPublic,
      });
      toast.success('Product Updated');
    } catch (error) {
      console.log(error);
      toast.error('unable to add product');
    }
  }

  const validFileTypes = ['image/jpg', 'image/jpeg', 'image/png'];
  const handleImage = (e) => {
    setFile(e.target.files[0]);
    // if(!validFileTypes.find(type=> type===file.type )){
    //     setError('File must be Image file');
    //     return;
    // }
    console.log(file);
  };

  const removeListing = async () => {
    console.log('remove listing called');
    try {
      res = await axios.post('/api/removelisting', {
        id,
      });
      toast.success('Item removed from Listing');
    } catch (error) {
      toast.error('could not perform action');
    }
  };
  const addListing = async () => {
    try {
      res = await axios.post('/api/addlisting', {
        id,
      });
      toast.success('Item added to Listing');
    } catch (error) {
      toast.error('could not perform action');
    }
  };

  return (
    <div className='flex overflow-hidden'>
      <SidebarDashboard />
      <div className='text-gray-600 body-font overflow-hidden w-full'>
        <form
          onSubmit={handleSubmit(submitHandler)}
          className='h-screen overflow-scroll p-5'
        >
          <div>
            <div className='flex flex-col'>
              {/* Left Section */}
              <div className='w-full'>
                {!file ? (
                  <img
                    alt='default'
                    className='sm:w-1/2 sm:h-[400px] md:w-[300px] md:h-[400px] object-cover rounded'
                    src={product.image}
                  />
                ) : (
                  <Image
                    src={URL.createObjectURL(file)}
                    alt='Picture'
                    width={300}
                    height={300}
                  />
                )}

                <input
                  id='Image'
                  type='file'
                  onChange={handleImage}
                  className='mt-4 mb-4'
                />

                <div className='flex justify-between items-center mb-4'>
                  <h1 className='text-gray-900 text-xl title-font font-medium'>
                    Make Product Public
                  </h1>
                  <input
                    id='isPublic'
                    checked={isPublic}
                    type='checkbox'
                    onChange={() => setIsPublic(!isPublic)}
                    className='ml-4 mt-2 rounded-full'
                  />
                </div>
              </div>
              {/* Right Section */}
              <div className='w-full'>
                <div className='flex justify-between items-center mb-4'>
                  <h1 className='text-gray-900 text-xl title-font font-medium '>
                    Enter Product Name
                  </h1>
                  <input
                    value={name}
                    type='text'
                    onChange={(e) => setName(e.target.value)}
                    className='rounded-md w-[200px]'
                  />
                </div>

                <div className='flex justify-between mb-4'>
                  <h1 className='text-gray-900 text-xl title-font font-medium'>
                    Select Product Category
                  </h1>
                  <select
                    value={category}
                    onChange={(e) => getSubCat(e.target.value)}
                    className='rounded-md w-[200px]'
                  >
                    <option value={''}>-----</option>
                    {categories.map((cate,index) => (
                      <option key={cate.name} value={cate.name}>{cate.name}</option>
                    ))}
                  </select>
                </div>

                <div className='flex justify-between items-center mb-4'>
                  <h1 className='text-gray-900 text-xl title-font font-medium mb-1'>
                    Select Sub Category
                  </h1>
                  <select
                    id='SubCategory'
                    value={subcategory}
                    onChange={(e) => setSubCategory(e.target.value)}
                    className='rounded w-[200px]'
                  >
                    <option value={''}>-----</option>
                    {subCat.map((sub,index) => (
                      <option key={sub.subName} value={sub.subName}>{sub.subName}</option>
                    ))}
                  </select>
                </div>

                <div className='flex justify-between h-48 mb-4'>
                  <h1 className='text-gray-900 text-xl title-font font-medium mb-1'>
                    Enter Product Description
                  </h1>
                  <input
                    value={desc}
                    type='textarea'
                    rows='4'
                    cols='50'
                    onChange={(e) => setDesc(e.target.value)}
                    className='rounded border-2 w-[200px]'
                  />
                </div>

                <div className='flex justify-between items-center mb-4'>
                  <label className='font-bold text-xl'>Enter Price ₹</label>
                  <input
                    value={price}
                    type='text'
                    onChange={(e) => setPrice(e.target.value)}
                    className='rounded w-[200px]'
                  />
                </div>

                <div className='flex justify-between items-center mb-4'>
                  <label className='font-bold text-xl'>Set Lot Size</label>

                  <input
                    value={batch}
                    type='text'
                    onChange={(e) => setBatch(e.target.value)}
                    className='rounded w-[200px]'
                  />
                </div>

                <div className='flex justify-between items-center mb-4'>
                  <label className='font-bold text-xl'>
                    Set Total Quantity
                  </label>

                  <input
                    value={stock}
                    type='text'
                    onChange={(e) => setStock(e.target.value)}
                    className='rounded w-[200px]'
                  />
                </div>
                <div className='flex flex-row justify-center w-full mt-10'>
                  <button className='flex text-red-700 bg-[#F6DE8D] border-0 py-2 px-6 focus:outline-none hover:bg-amber-400 active:bg-amber-500 rounded'>
                    Update Product
                  </button>
                  {product.listing ? (
                    <button
                      className='ml-2 flex text-red-700 bg-[#F6DE8D] border-0 py-2 px-6 focus:outline-none hover:bg-amber-400 active:bg-amber-500 rounded'
                      onClick={() => removeListing()}
                    >
                      Remove Product
                    </button>
                  ) : (
                    <button
                      className='ml-2 flex text-red-700 bg-[#F6DE8D] border-0 py-2 px-6 focus:outline-none hover:bg-amber-400 active:bg-amber-500 rounded'
                      onClick={() => addListing()}
                    >
                      Add Product
                    </button>
                  )}
                </div>
              </div>
              {/*   <div className='green-100'>
                <div>
                  <ManageCatagories tooltip='Add New Category' />
                </div>
                <div>
                  <ManageCatagories tooltip='Add New Subcategory' />
                </div>
              </div> */}
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export async function getServerSideProps(context) {
  const { params } = context;
  const { slug } = params;
  await db.connect();
  const product = await Product.findOne({ slug }).lean();
  await db.disconnect();
  console.log(product);
  return {
    props: {
      product: product ? db.convertDocToObj(product) : null,
    },
  };
}
