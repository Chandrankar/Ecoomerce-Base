import '../styles/globals.css'
import React from 'react';
import {StoreProvider} from '../utils/Store';



export default function App({ Component, pageProps }:any) {
  return (
      <StoreProvider>
          <Component {...pageProps} />
      </StoreProvider>
  );
}