import db from "../../utils/db";
import Category from '../../models/Category';
import User from "../../models/User";
import Organization from "../../models/Organization";

const addnewUser = async(req,res) => {
    console.log(req.body)
    await db.connect();
    // const cat = await User.find({name: req.body.category}).lean();

    let my_user ={
        name: req.body.name,
        mobile_number: req.body.mobile_number,
        organization: req.body.organization,
        allowed_sign_in: req.body.allowed_sign_in,
        role: req.body.role
    }
    await db.connect();
    let result = await User.create(my_user)

    res.status(201).send(result);
}

export default addnewUser