import db from '../../utils/db'
import Product from '../../models/Product'

const removelisting = async (req,res) => {
    console.log(req.body)
    await db.connect()
    var query ={_id: req.body.id}
    const options = {upsert: true};
    var newValues ={$set:{listing: false}}
    
    
    console.log(newValues)
  
    const result = await Product.updateOne(query, newValues, options)
    res.status(201).send(result)
}

export default removelisting