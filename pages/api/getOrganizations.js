import db from "../../utils/db";
import Organization from "../../models/Organization";

async function getOrganizations(req,res) {
        await db.connect();
        const organizations = await Organization.find().lean();
        await db.disconnect()
        res.send(organizations);
}

export default getOrganizations