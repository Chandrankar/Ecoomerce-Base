import db from '../../utils/db';
import Product from '../../models/Product';

const updateinventory = async(req,res) => {
  console.log("Req body i sent",req.body)
  var items = req.body.items;
  var update;
  var newqty;
  var query;
  var newValues;
  var result;
  var options = {upsert: true};
  await db.connect();
  console.log("Focus here.......",items)
  items.map(async(item)=>{
    console.log("Called")
    console.log(item.quantity)
    update = await Product.findOne({_id: item._id})
    newqty = update?.countInStock - item.quantity;
    query ={_id: item._id}
    
    newValues ={$set:{countInStock: newqty}}
    result = await Product.updateOne(query, newValues, options)
    update = {}
    newqty= 0;
    console.log(result)
  })
  await db.disconnect()
  res.status(201).send("success");
}

export default updateinventory