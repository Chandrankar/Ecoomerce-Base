import User from '../../models/User'
import db from '../../utils/db'

const verifyPh = async(req,res) => {
    console.log(req.body.phone)
    const ph= req.body.phone;
    await db.connect();
    const result = await User.findOne({mobile_number: ph}).lean();
    console.log(result)
    const id = result._id;
    const verified = result.isValid;
    res.send({id,verified})
}

export default verifyPh