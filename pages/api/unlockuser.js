import db from '../../utils/db'
import User from '../../models/User'

const lockuser = async(req,res) => {
    await db.connect()
    const result = await User.updateOne({mobile_number: req.body.ph},{allowed_sign_in: true},{upsert: true})
    await db.disconnect()
    res.status(201).send(result)
}

export default lockuser