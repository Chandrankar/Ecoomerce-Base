import db from "../../utils/db";
import Order from "../../models/Order";

async function getUserOrgs(req,res) {
        await db.connect();
        var id = req.params.id

        const user_orgs = await OrganizationUserMap.findOne({userID: id}).lean();
        await db.disconnect()
        res.send(user_orgs);
}

export default getUserOrgs