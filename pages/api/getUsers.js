import db from "../../utils/db";
import User from "../../models/User";

async function getUsers(req,res) {
        await db.connect();
        const users = await User.find().lean();
        await db.disconnect()
        res.send(users);
}

export default getUsers