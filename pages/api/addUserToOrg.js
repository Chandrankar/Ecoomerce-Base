import db from "../../utils/db";
import Category from '../../models/Category';
import User from "../../models/User";
import Organization from "../../models/Organization";
import OrganizationUserMap from "../../models/OrganizationUserMap";

const addUsertoOrg = async(req,res) => {
    console.log(req.body)
    await db.connect();
    // const cat = await User.find({name: req.body.category}).lean();

    var userOrgMap ={
        organizationID: req.body.organizationID,
        userID: req.body.userID

    }
    await db.connect();
    let result = await OrganizationUserMap.create(userOrgMap)

    res.status(201).send(result);
}

export default addUsertoOrg