import React from 'react'
import User from '../../models/User'
import db from '../../utils/db'


const edituser = async(req,res) => {
    try {
        await db.connect()
        console.log(req.body)
        const query ={_id : req.body.userid};
        var newvalues={
            name: req.body.user,
            role: req.body.role,
            mobile_number: req.body.mobile_number,
            PAN: req.body.PAN,
            GST: req.body.GST,
            Dhara: req.body.Dhara,
            Ledger_Type: req.body.Ledger_Type,
            TDS: req.body.TDS,
            TCS: req.body.TCS,
            Broker: req.body.Broker,
            Market: req.body.Market,
            Address: req.body.Address,
            Alternate_Address: req.body.Alternate_Address,
            Banks: req.body.Banks,
            Lower_TDS_Job_Amount: req.body.Lower_TDS_Job_Amount,
            Insurance_Policy_Number: req.body.Insurance_Policy_Number,
            isValid: true,
            Series: req.body.Series,
        }
        const options = {upsert: false};
        const result = await User.updateOne(query, newvalues, options)
        await db.disconnect()
        res.status(200)
    } catch (error) {
        res.status(401)
    }
}

export default edituser