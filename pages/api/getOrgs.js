import db from "../../utils/db";
import Organization from "../../models/Organization";

async function getOrgs(req,res) {
        await db.connect();
        const orgs = await Organization.find().lean();
        await db.disconnect()
        res.send(orgs);
}

export default getOrgs