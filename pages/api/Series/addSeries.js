import db from '../../../utils/db'
import Meta from '../../../models/Meta'

const addSeries = async(req,res) => {
    
    await db.connect()
    console.log(req.body)
    const query={_id:"644a57af1f88c8a7b5a8275e"}
    const meta = await Meta.findOne(query);
    var newvals = {$set:{Series:[...meta.Series, {Series_Name:req.body.name}]}}
    const options={upsert: true};
    const result = await Meta.updateOne(query,newvals,options)
    console.log(result)
    await db.disconnect()
    res.send(200)
}

export default addSeries