import db from "../../utils/db";
import Order from "../../models/Order";
import User from "../../models/User";

async function getUserDetails(req,res) {
        await db.connect();
        let mobile_number = req.body.mobile_number

        const _user = await User.findOne({mobile_number: mobile_number}).lean();
        await db.disconnect()
        res.send(_user);
}

export default getUserDetails