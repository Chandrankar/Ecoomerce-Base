import db from '../../utils/db';
import Specials from '../../models/Specials';
import User from '../../models/User';

const addtospl = async(req,res) => {
    console.log(req.body.product)
    await db.connect()
    const user = await User.findOne({_id: req.body.user_id})
    const Mob = user.mobile_number;
    const MyVal={
        userid: req.body.user_id,
        product:
        {
            Proid:req.body.product._id,
            name: req.body.product.name,
            slug: req.body.product.slug,
            category: req.body.product.category,
            subCategory:req.body.product.subCategory,
            image: req.body.product.image,
            price: req.body.product.price,
            batchSize: req.body.product.batchSize,
            countInStock: req.body.product.countInStock,
            description: req.body.product.description,
            isPublic: req.body.product.isPublic,
            listing : req.body.product.listing,
          },
        mobile_number: Mob
    }
    const result = await Specials.create(MyVal)
    res.status(201).send(result)
}

export default addtospl