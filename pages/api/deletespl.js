import db from '../../utils/db'
import Specials from '../../models/Specials'

const deletespl = async(req,res) => {
    console.log("body at delete",req.body)
    await db.connect();
    const result = await Specials.deleteOne({_id: req.body.id})
    await db.disconnect()
    res.status(201).send(result)
}

export default deletespl