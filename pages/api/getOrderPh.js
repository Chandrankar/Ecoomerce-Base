import db from "../../utils/db";
import Order from "../../models/Order";

async function getOrderPh(req,res) {
        await db.connect();
        const orders = await Order.find({"shippingAddress.phone": req.body.phone}).lean();
        await db.disconnect()
        res.send(orders);
}

export default getOrderPh