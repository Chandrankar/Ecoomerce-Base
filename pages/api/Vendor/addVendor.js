import db from '../../../utils/db'
import Meta from '../../../models/Meta'

const addVendor = async(req,res) => {
    
    await db.connect()
    const query={_id:"644a57af1f88c8a7b5a8275e"}
    const meta = await Meta.findOne(query);
    var newvals = {$set:{Transport_Vendor:[...meta.Transport_Vendor, {name:req.body.name}]}}
    const options={upsert: true};
    const result = await Meta.updateOne(query,newvals,options)
    res.send(200)
}

export default addVendor