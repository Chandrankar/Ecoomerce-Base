import db from "../../utils/db";
import Order from "../../models/Order";
import OrganizationUserMap from "../../models/OrganizationUserMap";

async function getUsersInOrg(req,res) {
        await db.connect();
        var id = req.body.id

        const org_users = await OrganizationUserMap.findOne({organizationID: id}).lean();
        await db.disconnect()
        res.send(org_users);
}

export default getUsersInOrg