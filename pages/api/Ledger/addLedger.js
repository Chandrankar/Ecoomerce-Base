import db from '../../../utils/db'
import Meta from '../../../models/Meta'

const addLedger = async(req,res) => {
    
    await db.connect()
    const query={_id:"644a57af1f88c8a7b5a8275e"}
    const meta = await Meta.findOne(query);
    var newvals = {$set:{Ledger_Type:[...meta.Ledger_Type, {Ledger_T:req.body.name}]}}
    const options={upsert: true};
    const result = await Meta.updateOne(query,newvals,options)
    res.send(200)
}

export default addLedger