import db from '../../utils/db'
import Meta from '../../models/Meta'
import { ObjectId } from 'mongodb'

const deleteBroker = async(req,res) => {
    if(!req.body.id){
        return res.status(400).send("Market is Required")
    }
    console.log(req.body)
    await db.connect()
    const query={_id:"644a57af1f88c8a7b5a8275e"}

    const result = await Meta.updateOne(query,{$pull:{Market:{_id: new ObjectId(req.body.id)}}},{upsert:false})
    console.log(result)
    await db.disconnect()
    res.send(200)
}

export default deleteBroker