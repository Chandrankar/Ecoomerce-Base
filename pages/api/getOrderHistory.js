import React from 'react'
import db from '../../utils/db';
import Order from '../../models/Order'

const getOrderHistory = async (req,res) => {
    await db.connect()
    console.log(req.body.phone)
    const result = await Order.find({"shippingAddress.phone": req.body.phone}).lean()
    await db.disconnect()
    res.status(201).send(result)
}

export default getOrderHistory