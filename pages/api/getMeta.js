import db from '../../utils/db';
import Meta from '../../models/Meta';

const getMeta = async(req,res) => {
    await db.connect();
    const result = await Meta.find().lean();
    await db.disconnect();
    res.status(201).send(result)
}

export default getMeta