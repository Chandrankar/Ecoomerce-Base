import {storage} from '../../firebase/firebase.App';
import {ref,getMetadata } from 'firebase/storage';
const getmetadata = async(req,res) => {
   const metaref = ref(storage, `${req.body.fullPath}`)
      getMetadata(metaref).then((metadata) => {
      res.send(metadata)
})
}

export default getmetadata