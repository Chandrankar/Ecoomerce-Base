var aws = require('aws-sdk');
const region = "ap-south-1"
const accessKeyId = process.env.MY_AWS_ACCESS_KEY
const secretAccessKey = process.env.MY_AWS_SECRET_KEY
const name ="TestName"
  let s3 = new aws.S3({
        region,
        accessKeyId,
        secretAccessKey,
    })

const createBucket = async(req,res) => {
    const id = req.body.id
    console.log(id)
    var bucketParams = {
        Bucket : name,
      };
    
      s3.createBucket(bucketParams, function(err, data) {
        if (err) {
          console.log("Error", err);
        } else {
          console.log("Success", data.Location);
        }
      });
    
}

export default createBucket
