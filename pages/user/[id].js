import React from 'react';
import db from '../../utils/db';
import User from '../../models/User';
import Layout from '../../components/Layout/Layout';
import { ref, listAll } from 'firebase/storage';
import { storage } from '../../firebase/firebase.App';
import { useEffect, useState, useMemo } from 'react';
import { DataGrid } from '@mui/x-data-grid';
import SidebarDashboard from '../../components/Sidebar/sidebarDashboard';
import Uploadbutton from '../../components/Uploadbutton/Uploadbutton';
import Downloadfile from '../../components/downloadbutton/downloadfile';
import axios from 'axios';
import Deletebutton from '../../components/Deletebutton/Deletebutton';
import { initFirebase } from '../../firebase/firebase.App';
import { getAuth } from 'firebase/auth';
import { useAuthState } from 'react-firebase-hooks/auth';


export default function UserScreen(props) {
  const [files, setFiles] = useState([]);
  const [meta, setMeta] = useState([]);
  const org_id = props.user.organization;
  const user_id = props.user._id;

  useEffect(() => {
    const listRef = ref(
      storage,
      `${props.user.organization}/${props.user._id}`
    );
    function retrieveFiles() {
      listAll(listRef).then((res) => {
        setFiles(res.items);
        //console.log(res.items)
      });
    }
    retrieveFiles();
  }, []);

  useMemo(() => {
    files.forEach(async (ref) => {
      var result = await axios.post('/api/getmetadata', {
        fullPath: ref.fullPath,
        name: ref.name,
      });
      setMeta(meta=>[...meta, result.data]);
    });
  }, [files]);

  const columns = [
    { field: 'name', headerName: 'Name', width: 200 },
    { field: 'timeCreated', headerName: 'Date', width: 200 },
    { field: 'contentType', headerName: 'Type', width: 150 },
    { field: 'size', headerName: 'Size', width: 100 },
    {
      field: 'Download',
      headername: 'Download File',
      width: 100,
      renderCell: ({ row: { name } }) => {
        return (
          <div>
            <Downloadfile name={`${org_id}/${user_id}/${name}`} />
          </div>
        );
      },
    },
    {
      field: 'Delete',
      headername: 'Delete File',
      flex: 1,
      renderCell: ({ row: { fullPath } }) => {
        return (
          <div>
            <Deletebutton fullPath={fullPath} />
          </div>
        );
      },
    },
  ];

  return (
    <div className='flex'>
      <SidebarDashboard />
      <div className='mx-4 my-4 w-full overflow-x-scroll'>
        <Uploadbutton org_id={org_id} user_id={user_id} />
        {files && (
          <DataGrid
            columns={columns}
            rows={meta}
            getRowId={(met) => met.name}
          />
        )}
      </div>
    </div>
  );
}

export async function getServerSideProps(context) {
  const { params } = context;
  const { slug } = params;
  await db.connect();
  const user = await User.findOne({ slug }).lean();
  await db.disconnect();
  return {
    props: {
      user: user ? db.convertDocToObj(user) : null,
    },
  };
}
