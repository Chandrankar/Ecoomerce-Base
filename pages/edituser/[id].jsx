import React,{useState, useMemo,useEffect} from 'react'
import User from '../../models/User'
import db from '../../utils/db'
import axios from 'axios'
import {toast} from 'react-toastify'
import { useForm } from 'react-hook-form';
import { useRouter } from 'next/router'
import { States, Districts } from "state-district-component";
import CurrencySelect from '@paylike/react-currency-select'

export default function ProductScreen(props) {

  const styles = {
    // pass the styles in the style props of component
    width: "230px",
    outline: "none",
    padding: "10px",
    background: "rgb(227, 226, 226)",
    border: "none",
    borderRadius: "6px",
  };

  const onSelectedCurrency = currencyAbbrev => {
    debug(`Selected ${currencyAbbrev}`)
}

    const {push} = useRouter()
    const { users } = props;
    const [name, setName] = useState(users.name)
    const [ledger, setLedger] = useState(users.Ledger_Type)
    const [pan,setPan] = useState(users.PAN)
    const [gst,setGst] = useState(users.GST)
    const [dhara,setDhara] = useState(users.Dhara)
    const [role,setRole] = useState(users.role)
    const [ph,setPh] = useState(users.mobile_number)
    const [grace,setGrace] = useState(users.Grace_Days)
    const [tds,setTds] = useState(users.TDS)
    const [tcs,setTcs] = useState(users.TCS)
    const [broker,setBroker] = useState(users.Broker)
    const [market,setMarket] = useState(users.Market)
    const [series, setSeries] = useState(users.Series)

    const [curr,setCurr] = useState("")
    const [policy_number,setPolicy_number] = useState(users.Insurance_Policy_Number)
    const [ltds,setLtds] = useState(users.Lower_TDS_Job_Amount)

    const [address,setAddress] = useState({})
    const [aladdress,setAladdress] = useState({})

    const [line1,setLine1] = useState(users.Address?.Address_Line1)
    const [line2,setLine2] = useState(users.Address?.Address_Line2)
    const [city,setCity] = useState(users.Address?.City)
    const [state,setState] = useState(users.Address?.State)
    const [country,setCountry] = useState("India");
    const [pincode,setPincode] = useState(users.Address?.Pincode)
    const [distance,setDistance] = useState(users.Address?.Distance)
    const [nearest_Railway,setNearest_Railway] = useState(users.Address?.Nearest_Railway)
    const [transport_Vendor,setTransport_Vendor] = useState(users.Address?.Transport_Vendor)
    const [bankname,setBankname] = useState(users.Banks?.Bank_Name)
    const [bankacc,setBankacc] = useState(users.Banks?.Bank_Account_Number)
    const [bankbranch,setBankbranch] = useState(users.Banks?.Bank_Branch)
    const [bankifsc,setIfsc] = useState(users.Banks?.IFSC_CODE)

    const [aline1,setAline1] = useState(users.Alternate_Address?.Address_Line1)
    const [aline2,setAline2] = useState(users.Alternate_Aaddress?.Address_Line2)
    const [acity,setAcity] = useState(users.Alternate_Address?.City)
    const [astate,setAstate] = useState(users.Alternate_Address?.State)
    const [apincode,setApincode] = useState(users.Alternate_Address?.Pincode)
    const [adistance,setAdistance] = useState(users.Alternate_Address?.Distance)
    const [anearest_Railway,setAnearest_Railway] = useState(users.Alternate_Address?.Nearest_Railway)
    const [atransport_Vendor,setAtransport_Vendor] = useState(users.Alternate_Address?.Transport_Vendor)

    const [showal, setShowal] = useState(false)

    const[meta,setMeta] = useState({})
    console.log(users.Address)
    const[ledgers,setLedgers] =useState([])
    const[markets,setMarkets] =useState([])
    const[brokers,setBrokers] =useState([])
    const [transport_vendors, setTransport_vendors] = useState([])
    const [serieses, setSerieses]= useState([])
    
    //const[ledgers,setLedgers] =useState([])
    useEffect(() => {
      async function getMeta(){
        const result = await axios.get('/api/getMeta')
        console.log(result.data)
        setLedgers(result.data[0].Ledger_Type)
        setMarkets(result.data[0].Market)
        setBrokers(result.data[0].Broker)
        setTransport_vendors(result.data[0].Transport_Vendor)
        setSerieses(result.data[0].Series)
        setMeta(result.data[0])
      }getMeta();
      }, [])
    

    const { handleSubmit } = useForm();
    
    const handleUseredit =async()=>{
        try {
            const result = axios.post('/api/edituser',{
              userid: users._id,
              user: name,
              role: role,
              mobile_number: ph,
              PAN: pan,
              GST: gst,
              Dhara: dhara,
              Ledger_Type: ledger,
              TDS: tds,
              TCS: tcs,
              Broker: broker,
              Market: market,
              Series: series,
              Address:{
                Address_Line1: line1,
                Address_Line2: line2,
                City: city,
                State: state,
                Country: country,
                Pincode: pincode,
                Distance: distance,
                Nearest_Railway: nearest_Railway,
                Transport_Vendor: transport_Vendor
              },
              Alternate_Address:{
                Address_Line1: aline1,
                Address_Line2: aline2,
                City: acity,
                State: astate,
                Country: country,
                Pincode: apincode,
                Distance: adistance,
                Nearest_Railway: anearest_Railway,
                Transport_Vendor: atransport_Vendor
              },
              Banks:{
                Bank_Account_Number: bankacc,
                Bank_Name: bankname,
                Bank_Branch: bankbranch,
                IFSC_Code: bankifsc
              },
              Insurance_Policy_Number: policy_number,
              Lower_TDS_Job_Amount: {
                Currency:curr,
                Amount: ltds}
            })
            toast.success("User Details Updated")
            push('/dashboard')
        } catch (error) {
            toast.error('Cannot make changes')
        }
    }



  const getStateValue = (value) => {
    // for geting  the input value pass the function in oChnage props and you will get value back from component
    setState(value);
  };
  const getDistrictValue = (value) => {
    setCity(value);
  };
  const getalStateValue = (value) => {
    // for geting  the input value pass the function in oChnage props and you will get value back from component
    setAstate(value);
  };
  const getalDistrictValue = (value) => {
    setAcity(value);
  };
    return (
      <div>
        <form
          onSubmit={handleSubmit(handleUseredit)}
          className='h-screen overflow-scroll p-5'
        >
            <div className='flex justify-between items-center mb-4'>
                  <h1 className='text-gray-900 text-xl title-font font-medium '>
                    Enter User Name
                  </h1>
                  <input
                    value={name}
                    type='text'
                    placeholder={users.name}
                    onChange={(e) => setName(e.target.value)}
                    className='rounded-md w-[200px]'
                  />
            </div>
            <div className='flex justify-between items-center mb-4'>
                  <h1 className='text-gray-900 text-xl title-font font-medium '>
                    Select Ledger Type
                  </h1>
                  <select
                      value={ledger}
                      onChange={(e) =>setLedger(e.target.value)}
                      className='rounded-md mx-2'
                      >
                      <option key="EmptyGrade" value="">---------</option>
                      {ledgers.map((bro,index)=>(
                        <option key={bro._id} value={bro.Ledger_T}>{bro.Ledger_T}</option>
                        ))}
                      </select>
            </div>
            <div className='flex justify-between items-center mb-4'>
                  <h1 className='text-gray-900 text-xl title-font font-medium '>
                    Enter PAN Number
                  </h1>
                  <input
                    value={pan}
                    type='text'
                    placeholder={users.PAN}
                    onChange={(e) => setPan(e.target.value)}
                    className='rounded-md w-[200px]'
                  />
            </div>
            <div className='flex justify-between items-center mb-4'>
                  <h1 className='text-gray-900 text-xl title-font font-medium '>
                    Enter GST Number
                  </h1>
                  <input
                    value={gst}
                    type='text'
                    placeholder={users.GST}
                    onChange={(e) => setGst(e.target.value)}
                    className='rounded-md w-[200px]'
                  />
            </div>
            <div className='flex justify-between items-center mb-4'>
                  <h1 className='text-gray-900 text-xl title-font font-medium '>
                    Enter Dhara Number
                  </h1>
                  <input
                    value={dhara}
                    type='text'
                    placeholder={users.Dhara}
                    onChange={(e) => setDhara(e.target.value)}
                    className='rounded-md w-[200px]'
                  />
            </div>
            <div className='flex justify-between items-center mb-4'>
                  <h1 className='text-gray-900 text-xl title-font font-medium '>
                    Select Role
                  </h1>
                  <select
                      value={role}
                      onChange={(e) =>setRole(e.target.value)}
                      className='rounded-md mx-2'
                      >
                        <option key="EmptyGrade" value="">---------</option>
                        <option key="role-admin" value="Admin">Admin</option>
                        <option key="role-manager" value="Manager">Manager</option>
                        <option key="role-staff" value="Staff">Staff</option>
                        <option key="role-user" value="User">User</option>
                      </select>
            </div>
            <div className='flex justify-between items-center mb-4'>
                  <h1 className='text-gray-900 text-xl title-font font-medium '>
                    Enter Grace Days
                  </h1>
                  <input
                    value={grace}
                    type='text'
                    placeholder={users.Grace_Days}
                    onChange={(e) => setGrace(e.target.value)}
                    className='rounded-md w-[200px]'
                  />
            </div>
            <div className='flex justify-between items-center mb-4'>
                  <h1 className='text-gray-900 text-xl title-font font-medium '>
                    Enter TDS
                  </h1>
                  <input
                    value={tds}
                    type='text'
                    placeholder={users.TDS}
                    onChange={(e) => setTds(e.target.value)}
                    className='rounded-md w-[200px]'
                  />
            </div>
            <div className='flex justify-between items-center mb-4'>
                  <h1 className='text-gray-900 text-xl title-font font-medium '>
                    Enter TCS
                  </h1>
                  <input
                    value={tcs}
                    type='text'
                    placeholder={users.TCS}
                    onChange={(e) => setTcs(e.target.value)}
                    className='rounded-md w-[200px]'
                  />
            </div>
            <div className='flex justify-between items-center mb-4'>
                  <h1 className='text-gray-900 text-xl title-font font-medium '>
                    Select Broker
                  </h1>
                  <select
                      value={broker}
                      onChange={(e) =>setBroker(e.target.value)}
                      className='rounded-md mx-2'
                      >
                      <option key="EmptyBroker" value="">---------</option>
                      {brokers.map((bro,index)=>(
                        <option key={bro._id} value={bro.name}>{bro.name}</option>
                        ))}
                      </select>
            </div>
            <div className='flex justify-between items-center mb-4'>
                  <h1 className='text-gray-900 text-xl title-font font-medium '>
                    Select Market
                  </h1>
                  <select
                      value={market}
                      onChange={(e) =>setMarket(e.target.value)}
                      className='rounded-md mx-2'
                      >
                      <option key="EmptyMarket" value="">---------</option>
                      {markets.map((bro,index)=>(
                        <option key={bro._id} value={bro.Market_Name}>{bro.Market_Name}</option>
                        ))}
                      </select>
            </div>
            <div className='flex justify-between items-center mb-4'>
                  <h1 className='text-gray-900 text-xl title-font font-medium '>
                    Select Series
                  </h1>
                  <select
                      value={series}
                      onChange={(e) =>setSeries(e.target.value)}
                      className='rounded-md mx-2'
                      >
                      <option key="EmptySeries" value="">---------</option>
                      {markets.map((bro,index)=>(
                        <option key={bro._id} value={bro.Series_Name}>{bro.Series_Name}</option>
                        ))}
                      </select>
            </div>
            <div className=" mb-4">
                  <h1 className='text-gray-900 text-xl title-font font-medium '>
                    Enter Address
                  </h1>
                  <div className='flex justify-between items-center mb-4 ml-4'>
                  <h2 className='text-gray-900 text-xl title-font font-small '>
                    Line 1
                  </h2>
                  <input
                    value={line1}
                    type='text'
                    placeholder={address?.Address_Lin1}
                    onChange={(e) => setLine1(e.target.value)}
                    className='rounded-md w-[200px]'
                  />
                 </div>
                 <div className='flex justify-between items-center mb-4 ml-4'>
                  <h2 className='text-gray-900 text-xl title-font font-small '>
                    Line 2
                  </h2>
                  <input
                    value={line2}
                    type='text'
                    placeholder={address?.Address_Line2}
                    onChange={(e) => setLine2(e.target.value)}
                    className='rounded-md w-[200px]'
                  />
                 </div>
                 <div className='flex justify-between items-center mb-4 ml-4'>
                  <h2 className='text-gray-900 text-xl title-font font-small '>
                    State
                  </h2>
                  <States styles={styles} onChange={getStateValue} />
                 </div>
                 <div className='flex justify-between items-center mb-4 ml-4'>
                  <h2 className='text-gray-900 text-xl title-font font-small '>
                    City
                  </h2>
                  <Districts state={state} style={styles} onChange={getDistrictValue} />
                 </div>
                 <div className='flex justify-between items-center mb-4 ml-4'>
                  <h2 className='text-gray-900 text-xl title-font font-small '>
                    Pincode
                  </h2>
                  <input
                    value={pincode}
                    type='text'
                    placeholder={address?.Pincode}
                    onChange={(e) => setPincode(e.target.value)}
                    className='rounded-md w-[200px]'
                  />
                 </div>
                 <div className='flex justify-between items-center mb-4 ml-4'>
                  <h2 className='text-gray-900 text-xl title-font font-small '>
                    Nearest Railway Station
                  </h2>
                  <input
                    value={nearest_Railway}
                    type='text'
                    placeholder={address?.Nearest_Railway}
                    onChange={(e) => setNearest_Railway(e.target.value)}
                    className='rounded-md w-[200px]'
                  />
                 </div>
                 <div className='flex justify-between items-center mb-4 ml-4'>
                  <h2 className='text-gray-900 text-xl title-font font-small '>
                    Distance
                  </h2>
                  <input
                    value={distance}
                    type='text'
                    placeholder={address?.Distance}
                    onChange={(e) => setDistance(e.target.value)}
                    className='rounded-md w-[200px]'
                  />
                 </div>
                 <div className='flex justify-between items-center mb-4'>
                  <h2 className='text-gray-900 text-xl title-font font-small ml-4 '>
                    Select Transport Vendor
                  </h2>
                  <select
                      value={transport_Vendor}
                      onChange={(e) =>setTransport_Vendor(e.target.value)}
                      className='rounded-md mx-2'
                      >
                      <option key="EmptyTV" value="">---------</option>
                      {transport_vendors.map((bro,index)=>(
                        <option key={bro._id} value={bro.name}>{bro.name}</option>
                        ))}
                      </select>
            </div>
            </div>
            <div className='flex mb-4'>
                  <h1 className='text-gray-900 text-xl title-font font-medium '>
                    Add Alternate Address
                  </h1>
                  <input
                    value={name}
                    type='checkbox'
                    onChange={(e) => setShowal(!showal)}
                    className='rounded-full ml-4 mt-2'
                  />
            </div>
            {showal && <div className=" mb-4">
                  <h1 className='text-gray-900 text-xl title-font font-medium '>
                    Enter Alternate Address
                  </h1>
                  <div className='flex justify-between items-center mb-4 ml-4'>
                  <h2 className='text-gray-900 text-xl title-font font-small '>
                    Line 1
                  </h2>
                  <input
                    value={aline1}
                    type='text'
                    placeholder={aladdress?.Lin1}
                    onChange={(e) => setAline1(e.target.value)}
                    className='rounded-md w-[200px]'
                  />
                 </div>
                 <div className='flex justify-between items-center mb-4 ml-4'>
                  <h2 className='text-gray-900 text-xl title-font font-small '>
                    Line 2
                  </h2>
                  <input
                    value={aline2}
                    type='text'
                    placeholder={aladdress?.Line2}
                    onChange={(e) => setAline2(e.target.value)}
                    className='rounded-md w-[200px]'
                  />
                 </div>
                 <div className='flex justify-between items-center mb-4 ml-4'>
                  <h2 className='text-gray-900 text-xl title-font font-small '>
                    State
                  </h2>
                  <States styles={styles} onChange={getalStateValue} />
                 </div>
                 <div className='flex justify-between items-center mb-4 ml-4'>
                  <h2 className='text-gray-900 text-xl title-font font-small '>
                    City
                  </h2>
                  <Districts state={state} style={styles} onChange={getalDistrictValue} />
                 </div>
                 <div className='flex justify-between items-center mb-4 ml-4'>
                  <h2 className='text-gray-900 text-xl title-font font-small '>
                    Pincode
                  </h2>
                  <input
                    value={apincode}
                    type='text'
                    placeholder={aladdress?.Pincode}
                    onChange={(e) => setApincode(e.target.value)}
                    className='rounded-md w-[200px]'
                  />
                 </div>
                 <div className='flex justify-between items-center mb-4 ml-4'>
                  <h2 className='text-gray-900 text-xl title-font font-small '>
                    Nearest Railway Station
                  </h2>
                  <input
                    value={anearest_Railway}
                    type='text'
                    placeholder={aladdress?.Nearest_Railway}
                    onChange={(e) => setAnearest_Railway(e.target.value)}
                    className='rounded-md w-[200px]'
                  />
                 </div>
                 <div className='flex justify-between items-center mb-4 ml-4'>
                  <h2 className='text-gray-900 text-xl title-font font-small '>
                    Distance
                  </h2>
                  <input
                    value={adistance}
                    type='text'
                    placeholder={aladdress?.Distance}
                    onChange={(e) => setAdistance(e.target.value)}
                    className='rounded-md w-[200px]'
                  />
                 </div>
                 <div className='flex justify-between items-center mb-4'>
                  <h2 className='text-gray-900 text-xl title-font font-small ml-4 '>
                    Select Transport Vendor
                  </h2>
                  <select
                      value={atransport_Vendor}
                      onChange={(e) =>setAtransport_Vendor(e.target.value)}
                      className='rounded-md mx-2'
                      >
                      <option key="EmptyATV" value="">---------</option>
                      {transport_vendors.map((bro,index)=>(
                        <option key={bro._id} value={bro.name}>{bro.name}</option>
                        ))}
                      </select>
            </div>
            </div>}
            <div className='mb-4'>
                  <h1 className='text-gray-900 text-xl title-font font-medium '>
                    Enter Banking Details
                  </h1>
                  <div className='flex justify-between items-center mb-4'>
                  <h2 className='text-gray-900 text-xl title-font font-small ml-4 '>
                    Enter Account Number
                  </h2>
                  <input
                    value={bankacc}
                    type='text'
                    placeholder={users.Banks?.Alternate_Address}
                    onChange={(e) => setBankacc(e.target.value)}
                    className='rounded-md w-[200px]'
                  />
                  </div>
                  <div className='flex justify-between items-center mb-4'>
                  <h2 className='text-gray-900 text-xl title-font font-small ml-4 '>
                    Enter Bank Name
                  </h2>
                  <input
                    value={bankname}
                    type='text'
                    placeholder={users.Banks?.Bank_Name}
                    onChange={(e) => setBankname(e.target.value)}
                    className='rounded-md w-[200px]'
                  />
                  </div>
                  <div className='flex justify-between items-center mb-4'>
                  <h2 className='text-gray-900 text-xl title-font font-small ml-4 '>
                    Enter Bank Branch
                  </h2>
                  <input
                    value={bankbranch}
                    type='text'
                    placeholder={users.Banks?.Bank_Branch}
                    onChange={(e) => setBankbranch(e.target.value)}
                    className='rounded-md w-[200px]'
                  />
                  </div>
                  <div className='flex justify-between items-center mb-4'>
                  <h2 className='text-gray-900 text-xl title-font font-small ml-4 '>
                    Enter IFSC Code
                  </h2>
                  <input
                    value={bankifsc}
                    type='text'
                    placeholder={users.Banks?.IFSC_CODE}
                    onChange={(e) => setIfsc(e.target.value)}
                    className='rounded-md w-[200px]'
                  />
                  </div>
            </div>
            <div className='flex justify-between items-center mb-4'>
                  <h1 className='text-gray-900 text-xl title-font font-medium '>
                    Insurance Policy Number
                  </h1>
                  <input
                    value={policy_number}
                    type='text'
                    placeholder={users.Insurance_Policy_Number}
                    onChange={(e) => setPolicy_number(e.target.value)}
                    className='rounded-md w-[200px]'
                  />
            </div>
            <div className='flex justify-between items-center mb-4'>
                  <h1 className='text-gray-900 text-xl title-font font-medium '>
                    Lower TDS Job Amount
                  </h1>
                  <CurrencySelect onChange={(currency)=>setCurr(currency)}/>
                  <input
                    value={ltds}
                    type='text'
                    placeholder={users.Lower_TDS_Job_Amount}
                    onChange={(e) => setLtds(e.target.value)}
                    className='rounded-md w-[200px]'
                  />
            </div>
            <button className="primary-button">Add Details</button>
        </form>
      </div>
    );
  }
  
  export async function getServerSideProps(context) {
    const { params } = context;
    const { slug } = params;
    const userid = params.id;
    await db.connect();
    const users = await User.findOne({ "_id": userid }).lean();
    await db.disconnect();
    return {
      props: {
        users: users ? db.convertDocToObj(users) : null,
      },
    };
  }