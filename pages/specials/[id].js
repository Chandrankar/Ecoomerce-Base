import React from 'react'
import Specials from '../../models/Specials'
import Product from '../../models/Product'
import db from '../../utils/db'
import Sidebarproducts from '../../components/Sidebar/Sidebarproducts'
import {useRouter} from 'next/router';
import Specialitem from '../../components/Specialitem';
import Layout from '../../components/Layout/Layout'
import SidebarDashboard from '../../components/Sidebar/sidebarDashboard'
import { initFirebase } from '../../firebase/firebase.App';
import { getAuth } from 'firebase/auth';
import { useAuthState } from 'react-firebase-hooks/auth';
import axios from 'axios'
import { useState } from 'react'
import { useMemo } from 'react'
  


const Specialspage = (props) => {
    const router = useRouter();
    const user_id = router.query.id;
    const specials = props.specials;
    const app = initFirebase();
    const auth = getAuth();
    const [user] = useAuthState(auth);
    const [userdet,setUserdet] = useState({})

    async function getUser(){
      try{
        var phone_number = user.phoneNumber
        var ph = phone_number.slice(1)
        const result = await axios.post('/api/getUserDetails',{mobile_number: ph})
        setUserdet(result.data)
      }catch(error){
        console.log(error)
      }
    }
    useMemo(() => getUser(), [user])

  return (
    <Layout><div className="flex">
      <SidebarDashboard/>
      <div className="ml-4 mt-4">{userdet.role =="Admin" && <Sidebarproducts products={props.products} user_id={user_id} />}
      <div className="grid grid-cols-1 gap-4 md:grid-cols-3">
          {specials.map((pro)=>(
            <Specialitem
              key={pro._id}
              product={pro}
              className="cols-span-3"/>
          ))}
          </div>
      </div>
     
      </div>
    </Layout>
  )
}

export async function getServerSideProps(context) {
    const { params } = context;
    const { slug } = params;
    await db.connect();
    const specials = await Specials.find({ slug }).lean();
    const products = await Product.find({isPublic: false,listing:true}).lean();
    await db.disconnect();
    return {
        props: {
         specials: specials.map(db.convertDocToObj),
         products: products.map(db.convertDocToObj),
        },
     };
  }

export default Specialspage