import React,{useEffect, useState} from 'react';
import Layout from '../components/Layout/Layout'
import SidebarDashboard from '../components/Sidebar/sidebarDashboard';
import {useForm} from 'react-hook-form';
import {toast} from 'react-toastify';
import { useRouter } from 'next/router';
import ManageCatagories from '../components/manageCategories';
import { v4 as uuidv4 } from 'uuid';
import { db } from '../firebase/firebase.App';
import {addDoc, collection} from "@firebase/firestore"
import { storage } from '../firebase/firebase.App'
import { Button, Card, Input, List, message, Image, Progress } from 'antd'
import { ref, uploadBytesResumable, getDownloadURL } from 'firebase/storage'

const Addproduct = () => {

    const {push} = useRouter();
    const{handleSubmit, setValue} = useForm();

    const[name, setName] = useState<string>('');
    const [category, setCategory] = useState<string>('');
    const [subcategory, setSubCategory] = useState<string>('');
    const [isPublic, setIsPublic] = useState<boolean>(false);
    const [stock,setStock] = useState(0);
    const [batch, setBatch] = useState(1);
    const [desc , setDesc] = useState('');
    const[price, setPrice] = useState(0);
    const [error, setError] = useState('');
    const[ categories, setCategories]=useState([])
    const [subCat, setSubCat]= useState([]);

    const [imageFile, setImageFile] = useState<File>()
    const [downloadURL, setDownloadURL] = useState('')
    const [isUploading, setIsUploading] = useState(false)
    const [progressUpload, setProgressUpload] = useState(0)


  const handleUploadFile = () => {
    const fname= uuidv4()
    if (imageFile) {
      const name = imageFile.name
      const storageRef = ref(storage, `Products/${fname}`)
      const uploadTask = uploadBytesResumable(storageRef, imageFile)

      uploadTask.on(
        'state_changed',
        (snapshot) => {
          const progress =
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100

          setProgressUpload(progress) // to show progress upload

          switch (snapshot.state) {
            case 'paused':
              console.log('Upload is paused')
              break
            case 'running':
              console.log('Upload is running')
              break
          }
        },
        (error) => {
          message.error(error.message)
        },
        () => {
          getDownloadURL(uploadTask.snapshot.ref).then((url) => {
            //url is download url of file
            setDownloadURL(url)
            console.log(url)
          })
        },
      )
    } else {
      message.error('File not found')
    }
  }
  const handleSelectedFile = (files: any) => {
    if (files && files[0].size < 10000000) {
      setImageFile(files[0])
      console.log(files[0])
    } else {
      message.error('File size to large')
    }
  }

  const productCollectionRef = collection(db, "Product")

  const createProduct = async()=>{
    var uid = uuidv4()
    let slug = "p"+"-"+ name.split(" ")[0]+"-"+uid;
    try{
    await addDoc(productCollectionRef,{batchSize: Number(batch), category: category, subcategory: subcategory, countInStock: Number(stock),
                  description: desc, image: downloadURL, isPublic: Boolean(isPublic), listing: Boolean(true),name: name, price: Number(price), slug: slug
    })}
    catch(error){
      toast.error("Cannot Add Product")
    }
  }

  
   
  return (
    <Layout title="Add Product">
    <div className="flex">
        <SidebarDashboard/>
        <section className="text-gray-600 body-font overflow-hidden w-full">
    <form onSubmit={handleSubmit(createProduct)}>
  <div className="container px-5 py-24 mx-auto">
    <div className="flex">
        <div className="w-full">
            {!imageFile? (<img alt="default" className="lg:w-1/2 w-full lg:h-auto h-64 object-cover object-center rounded" src="images/default.png"/>):
            <Image
            src={URL.createObjectURL(imageFile)}
            alt="Picture"
            width={300}
            height={300}
            />
            }
            
            <Input
              type="file"
              placeholder="Select file to upload"
              accept="image/png"
              onChange={(files) => handleSelectedFile(files.target.files)}
            />
            <button className="primary-button" onClick={()=>handleUploadFile()}>Upload File</button>
            <div className="flex  p-4">
        <h1 className="text-gray-900 text-xl title-font font-medium mb-1 mx-4">Make Product Public</h1>
        <input id="isPublic" type="checkbox"   onChange={(e)=>setIsPublic(!isPublic)} className="ml-4 mt-2 rounded-full"/>
        </div>
        </div>
      <div className="w-full lg:pl-10 lg:py-6 mt-6 lg:mt-0">
        <div className="flex justify-between p-4">
        <h1 className="text-gray-900 text-xl title-font font-medium mb-1 mx-4">Enter Product Name</h1>
        <input value={name} type="text" onChange={(e)=>setName(e.target.value)} className="rounded-md"/>
        </div>
        <div className="flex justify-between p-4">
        <h1 className="text-gray-900 text-xl title-font font-medium mb-1 mx-4">Select Product Category</h1>
        <select value={category} onChange={(e)=>getSubCat(e.target.value)}  className="rounded-md mx-2">
                        <option value={''}>-----</option>
                       {categories.map((cate,index)=>(
                        <option key={cate.name} value={cate.name}>
                            {cate.name}
                        </option>
                       ))}
                    </select>
        </div>
        <div className="flex justify-between p-4">
        <h1 className="text-gray-900 text-xl title-font font-medium mb-1 mx-4">Select Sub Category</h1>
        <select id="SubCategory" value={subcategory} onChange={(e)=>setSubCategory(e.target.value)} className="rounded-md mx-2">
                        <option value={''}>-----</option>
                        {   
                         subCat.map((sub,index)=>(
                                <option key={sub.subName} value={sub.subName}>{sub.subName}</option>
                         ))
                        }
                    </select>
        </div>

            <div className="flex justify-between h-48 p-4">
            <h1 className="text-gray-900 text-xl title-font font-medium mb-1 mx-4">Enter Product Description</h1>
            <input value={desc} type="textarea" rows="4" cols="50" onChange={(e)=>setDesc(e.target.value)} className="rounded-md border-2"/>
            </div>
          
        <div className="flex justify-between p-4">
        <label className="font-bold text-xl">
          Enter Price ₹</label>
          <input value={price} type="text" onChange={(e)=>setPrice(e.target.value)} className="rounded-md"/>
        </div>
        <div className="flex justify-between p-4">
        
             <label className="font-bold text-xl">Set Lot Size</label>
        
        <input value={batch} type="text" onChange={(e)=>setBatch(e.target.value)} className="rounded-md"/>
        </div>
        <div className="flex justify-between p-4">
      
             <label className="font-bold text-xl">Set Total Qunatity</label>
      
        <input value={stock} type="text" onChange={(e)=>setStock(e.target.value)} className="rounded-md"/>
        </div>
        <button className="flex text-red-700 bg-[#F6DE8D] border-0 py-2 px-6 focus:outline-none hover:bg-amber-400 active:bg-amber-500 rounded">Add Product</button>
      </div>
      <div className="green-100">
        <div className="py-8"></div>
        <div className="p-4"></div>
        <div className="py-4">
            <ManageCatagories tooltip="Add New Category"/>
        </div>
        <div className="py-4">
        <ManageCatagories tooltip="Add New Subcategory"/>
        </div>
        </div>
    </div>
  </div>
  </form>
</section>
    </div>
    </Layout>
  )
}



export default Addproduct
