import React, { useEffect, useState } from 'react';
import { db } from '../firebase/firebase.App';
import {collection, onSnapshot, orderBy, query, addDoc, updateDoc, doc } from "@firebase/firestore"
import ManageCatagories from '../components/manageCategories';

export default function test() {
  const [products, setProducts] = useState([])
  const [name, setName] = useState("")
  const [price,setPrice] = useState(0)

  const productCollectionRef = collection(db, "Categories")
  useEffect(() => {
    const collectionRef = collection(db, "Product")
    const q = query(collectionRef, orderBy("name"));

    const unsubscribe = onSnapshot(q,(querySnapshot)=>{
      setProducts(querySnapshot.docs.map(doc=>({...doc.data(),id: doc.id})))
    })
    return unsubscribe;
  }, [])
  console.log(products)
  const createProduct = async ()=> {
    await addDoc(productCollectionRef, {name: name, price: Number(price), isPublic:Boolean(true)})
  }
  const updateProduct = async (id, age)=> {
    const productDoc = doc(db,"Product", id)
    await updateDoc(productDoc, {name: name, price: Number(price), isPublic: Boolean(true)})
  }

  console.log(products)
  return (
    
    <div className="text-center flex flex-col gap-4 items-center">
      <ManageCatagories/>
      {products[0]?.subCategory?.map((pro,key)=>(<div key={key}>{pro}</div>))}
    </div>
    
  )
}
