import React, { useEffect, useState } from 'react';
import Hero from '../components/Hero/Hero';
import Collection from '../components/collections/collections';
import TryOutCollections from '../components/collections/TryOutCollections';
import Floatnav from '../components/Navbar/Floatnav';
import Productitem from '../components/Productitem';
import Footer from '../components/Footer/footer';
import { default as BottomNavbar } from '../components/Navbar/Floatnav';
import { db } from '../firebase/firebase.App';
import {collection, onSnapshot, orderBy, query, where } from "@firebase/firestore"

const Home = () => {
  const [products, setProducts] = useState([])

  useEffect(() => {
    const collectionRef = collection(db, "Product")
    const q = query(collectionRef, where("isPublic","==",true) , where("listing","==", true));

    const unsubscribe = onSnapshot(q,(querySnapshot)=>{
      setProducts(querySnapshot.docs.map(doc=>({...doc.data(),id: doc.id})))
      console.log(products)
    })
    return unsubscribe;
  }, [])
  return (
    <div className="flex flex-col justify-center items-center">
      <Floatnav/>
        <Hero/>
        <Collection/>
        <div className="w-3/4">
            <div className="flex justify-between">
                <div className="flex py-2 text-xl ml-4"><p>Grab the Best Deal on </p>&nbsp;<p className="text-red-700">Sarees</p></div>
                <div className="text-xl mr-4">View All</div>
            </div>
        
            <div className="grid grid-cols-1 md:grid-cols-3 lg:grid-cols-5">
            {products.map((product)=>(
            <Productitem product={product} key={product.slug}></Productitem>
          ))}
        </div>
      </div>

      <Footer />
    </div>
  );
};


export default Home;
