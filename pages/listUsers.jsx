import React, { useState, useEffect } from 'react';
import Layout from '../components/Layout/Layout';
import SidebarDashboard from '../components/Sidebar/sidebarDashboard';
import { DataGrid } from '@mui/x-data-grid';
import axios from 'axios';
import { useRouter } from 'next/router';
import { toast } from 'react-toastify';
import db from '../utils/db';
import User from '../models/User';

const ListUsers = (props) => {
  const { push } = useRouter();

  const columns = [
    { field: '_id', headerName: 'User Id', width: 100 },
    { field: 'name', headerName: 'Name', width: 100 },
    { field: 'mobile_number', headerName: 'Payment Status', width: 150 },
    { field: 'role', headerName: 'Role', width: 100 },
    { field: 'allowed_sign_in', headerName: 'Access', width: 100 },
    {
      field: 'details',
      headername: 'Show Details',
      width: 150,
      renderCell: ({ row: { _id } }) => {
        return (
          <button onClick={() => push(`/user/${_id}`)}>Show Details</button>
        );
      },
    },
  ];

  return (
    <div className='flex'>
      <SidebarDashboard />
      <div className='mx-4 my-4 w-full overflow-x-scroll'>
        <DataGrid
          columns={columns}
          rows={props.users}
          getRowId={(usr) => usr._id}
        />
      </div>
    </div>
  );
};

export async function getServerSideProps() {
  await db.connect();
  const users = await User.find().limit().lean();
  await db.disconnect();
  return {
    props: {
      users: users.map(db.convertDocToObj),
    },
  };
}

export default ListUsers;
