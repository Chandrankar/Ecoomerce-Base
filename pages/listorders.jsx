import React, { useEffect, useState } from 'react';
import Layout from '../components/Layout/Layout';
import SidebarDashboard from '../components/Sidebar/sidebarDashboard';
import { DataGrid, GridToolbar } from '@mui/x-data-grid';
import axios from 'axios';
import { useRouter } from 'next/router';
import { toast } from 'react-toastify';

const ListOrders = () => {
  const { push } = useRouter();
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    async function getcategories() {
      try {
        const ord = await axios.get('/api/getOrders');
        console.log(ord.data);
        setOrders(ord.data);
        //categories = cat.data
      } catch (error) {
        toast.error('something went wrong');
      }
    }
    getcategories();
  }, []);

  const columns = [
    { field: '_id', headerName: 'Order Id', width: 100 },
    { field: 'totalPrice', headerName: 'Total Price', width: 100 },
    { field: 'isPaid', headerName: 'Payment Status', width: 100 },
    { field: 'isDelivered', headerName: 'Delivery Status', width: 120 },
    { field: 'status', headerName: 'Status', width: 100 },
    { field: 'createdAt', headerName: 'Order Date', width: 100 },
    {
      field: 'details',
      headername: 'Show Details',
      width: 120,
      renderCell: ({ row: { _id } }) => {
        return (
          <div>
            <button onClick={() => push(`/order/${_id}`)}>Show Details</button>
          </div>
        );
      },
    },
  ];

  return (
    <div className='flex'>
      <SidebarDashboard />
      <div className='mx-4 my-4 w-full overflow-x-scroll'>
        <DataGrid
          columns={columns}
          rows={orders}
          getRowId={(ord) => ord._id}
          components={{ Toolbar: GridToolbar }}
        />
      </div>
    </div>
  );
};

export default ListOrders;
