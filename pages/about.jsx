import React from 'react';
import Navbar from '../components/Navbar/Navbar';

const About = () => {
  return (
    <div className='flex flex-col'>
      <Navbar />
      <div class='container mx-auto px-4 py-12 max-w-[600px]'>
        <div class='text-center'>
          <h1 class='text-4xl font-medium mb-8'>About Us</h1>
        </div>
        <div class='grid grid-cols-1 gap-8'>
          <div>
            {/* <img
              src='your-image-url.jpg'
              alt='About Us Image'
              class='w-full h-auto rounded shadow-md'
            /> */}
          </div>
          <div>
            <h2 class='text-2xl font-medium mb-4'>Our Story</h2>
            <p class='text-gray-600 mb-4'>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce non
              arcu non dolor fermentum malesuada vel eget ante. In hac habitasse
              platea dictumst. Morbi id tempor ligula. Fusce luctus venenatis
              lorem, eget sagittis mauris bibendum vel.
            </p>
            <p class='text-gray-600 mb-4'>
              Quisque eget dapibus ante, id pharetra lacus. Suspendisse ut
              ullamcorper massa, et bibendum nisi. In malesuada suscipit elit,
              at pulvinar diam lacinia id. Ut tempor tincidunt erat, ut interdum
              turpis malesuada vel.
            </p>
          </div>

          <div>
            <h2 class='text-2xl font-medium mb-4'>
              3 Decades & Still Going Strong!
            </h2>
            <p class='text-gray-600 mb-4'>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce non
              arcu non dolor fermentum malesuada vel eget ante. In hac habitasse
              platea dictumst. Morbi id tempor ligula. Fusce luctus venenatis
              lorem, eget sagittis mauris bibendum vel.
            </p>
            <p class='text-gray-600 mb-4'>
              Quisque eget dapibus ante, id pharetra lacus. Suspendisse ut
              ullamcorper massa, et bibendum nisi. In malesuada suscipit elit,
              at pulvinar diam lacinia id. Ut tempor tincidunt erat, ut interdum
              turpis malesuada vel.
            </p>
          </div>

          <div>
            <h2 class='text-2xl font-medium mb-4'>Happy Customers</h2>
            <p class='text-gray-600 mb-4'>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce non
              arcu non dolor fermentum malesuada vel eget ante. In hac habitasse
              platea dictumst. Morbi id tempor ligula. Fusce luctus venenatis
              lorem, eget sagittis mauris bibendum vel.
            </p>
            <p class='text-gray-600 mb-4'>
              Quisque eget dapibus ante, id pharetra lacus. Suspendisse ut
              ullamcorper massa, et bibendum nisi. In malesuada suscipit elit,
              at pulvinar diam lacinia id. Ut tempor tincidunt erat, ut interdum
              turpis malesuada vel.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default About;
