import React, { useState } from 'react';
import PhoneInput from 'react-phone-input-2';
import { RecaptchaVerifier } from 'firebase/auth';
import { auth } from '../firebase/firebase.App';
import { signInWithPhoneNumber } from 'firebase/auth';
import { useRouter } from 'next/router';
import axios from 'axios';
import Image from 'next/image';
import { toast } from 'react-toastify';
import TextField from '@mui/material/TextField';

const Login = () => {
  const [otp, setOtp] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [showOTP, setShowOTP] = useState(false);
  const [user, setUser] = useState(null);
  const { push } = useRouter();

  const handleOnChangeOTP = (e) => {
    setOtp(e.target.value);
  };

  const handleCaptchaVerify = () => {
    if (!window.recaptchaVerifier) {
      window.recaptchaVerifier = new RecaptchaVerifier(
        'recaptcha-container',
        {
          size: 'invisible',
          callback: (response) => {
            const appVerifier = window.recaptchaVerifier;

            const formatPh = '+' + phoneNumber;
            signInWithPhoneNumber(auth, formatPh, appVerifier)
              .then((confirmationResult) => {
                window.confirmationResult = confirmationResult;
                setShowOTP(true);
              })
              .catch((error) => {
                console.log(error);
              });
          },
          'expired-callback': () => {},
        },
        auth
      );
    }
  };

  const handleFirebaseSignup = () => {
    handleCaptchaVerify();
    const appVerifier = window.recaptchaVerifier;
    const formattedNumber = '+' + phoneNumber;

    signInWithPhoneNumber(auth, formattedNumber, appVerifier)
      .then((confirmationResult) => {
        window.confirmationResult = confirmationResult;
        setShowOTP(true);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleOTPVerification = () => {
    window.confirmationResult
      .confirm(otp)
      .then(async (result) => {
        console.log(result);
        setUser(result.user);
        const result2 = await axios.post('/api/isUserVerified',{phone: phoneNumber})
        var isVerified = result2.data.verified
        var uid = result2.data.id
        if(!isVerified)
        {push('/');}
        else{
          push(`/edituser/${uid}`)
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleVerify = async () => {
    const res = await axios.post('/api/verifyPh', {
      phone: phoneNumber,
    });

    if (res.data == 'Allowed') {
      handleFirebaseSignup();
    } else {
      toast.error('Login Failed!');
    }
  };

  return (
    <section className='bg-red-primary flex items-center justify-center h-screen'>
      <div>
        <div id='recaptcha-container'></div>
        {user ? (
          <div>
            <h2 className='text-center leading-normal text-white font-medium text-2xl'>
              Login Success
            </h2>
          </div>
        ) : (
          <div className='w-[600px] flex flex-col rounded-lg p-4'>
            <div className='flex justify-center'>
              <h1 className='text-center leading-normal text-white font-medium text-3xl mb-6'>
                <Image
                  src='/fav.ico'
                  alt='me'
                  width='250'
                  height='250'
                  className='object-contain'
                />
              </h1>
            </div>
            {showOTP ? (
              <>
                <p className='text-2xl text-white text-left'>
                  Please enter the One-Time code to verify your account
                </p>
                <p className='text-base text-white text-left mt-1'>
                  Sent to {phoneNumber}
                </p>

                <div className='my-6'>
                  <p className='text-sm text-white mb-2'>Enter OTP</p>
                  <TextField
                    id='outlined-basic'
                    label={otp.length ? '' : '6-digit code'}
                    variant='outlined'
                    fullWidth
                    size='small'
                    value={otp}
                    onChange={handleOnChangeOTP}
                    inputProps={{ maxLength: 6 }}
                    InputLabelProps={{ shrink: false }}
                  />
                </div>
                <button
                  className='primary-button mt-2'
                  onClick={handleOTPVerification}
                >
                  Submit
                </button>
              </>
            ) : (
              <>
                <p className='text-2xl text-white text-left '>Login</p>
                <p className='text-base text-white text-left mt-1'>
                  {`Enter your phone number and we'll send you a login code`}
                </p>

                <div className='w-full my-6'>
                  <p className='text-sm text-white mb-2'>Phone Number</p>
                  <PhoneInput
                    country={'in'}
                    value={phoneNumber}
                    onChange={setPhoneNumber}
                    placeholder='Enter phone number'
                    inputStyle={{ width: '100%' }}
                    containerStyle={{ margin: '0' }}
                    specialLabel=''
                  />
                </div>

                <button className='primary-button' onClick={handleVerify}>
                  Get OTP
                </button>
              </>
            )}
          </div>
        )}
      </div>
    </section>
  );
};

export default Login;
