import React,{useEffect} from 'react';
import SidebarDashboard from "../components/Sidebar/sidebarDashboard"
import Layout from '../components/Layout/Layout';
import { useState } from 'react';
import axios from 'axios';
import Addledgermodal from '../components/Modals/addledgermodal'
import Addbrokermodal from '../components/Modals/addbrokermodal'
import Addmarketmodal from '../components/Modals/addmarketmodal'
import Addvendormodal from '../components/Modals/addvendormodal'
import Addseriesmodal from '../components/Modals/addseriesmodal'

export default function Dashboard(){
  
  const[ledger, setLedger] = useState([])
  const[broker, setBroker] = useState([])
  const[market, setMarket] = useState([])
  const[series, setSeries] = useState([])
  const[transport, setTransport] = useState([])
  const[party, setParty] = useState([])

  const getMeta =async()=>{
    console.log('called')
    const result = await axios.get('/api/getMeta')
    setLedger(result.data[0].Ledger_Type)
    setBroker(result.data[0].Broker)
    setMarket(result.data[0].Market)
    setSeries(result.data[0].Series)
    setTransport(result.data[0].Transport_Vendor)
  }
  useEffect(() => {
    getMeta()
  }, [])
  return (
    <Layout>
      <div className="flex">
        <SidebarDashboard/>
        <div className="md:flex">
        <div className="card ml-4 overflow-x-auto p-5 divide-y-2 h-80 overflow-y-scroll">
          <div className="flex p-2"> <h1 className="text-xl font-bold mt-1 mr-2"> Ledger Types</h1><Addledgermodal/></div>
        {ledger.map((led,index)=>(<div className="flex flex-rows items-center" key={`${led.Ledger_T}+${index}`}>{led.Ledger_T}</div>))}
        </div>
        <div className="card ml-4 overflow-x-auto p-5 h-80 overflow-y-scroll divide-y-2">
          <div className="flex p-2"> <h1 className="text-xl font-bold mt-1 mr-2"> Brokers</h1><Addbrokermodal/></div>
        {broker.map((led,index)=>(<div className="flex flex-rows items-center" key={`${led.name}+${index}`}>{led.name}</div>))}
        </div>
        <div className="card ml-4 overflow-x-auto p-5 h-80 overflow-y-scroll divide-y-2">
          <div className="flex p-2"> <h1 className="text-xl font-bold mt-1 mr-2"> Markets</h1><Addmarketmodal/></div>
        {market.map((led,index)=>(<div className="flex flex-rows items-center" key={`${led.Market_Name}+${index}`}>{led.Market_Name}</div>))}
        </div>
        <div className="card ml-4 overflow-x-auto p-5 h-80 overflow-y-scroll divide-y-2">
          <div className="flex p-2"> <h1 className="text-xl font-bold mt-1 mr-2"> Transport Vendors</h1><Addvendormodal/></div>
        {transport.map((led,index)=>(<div className="flex flex-rows items-center" key={`${led.name}+${index}`}>{led.name}</div>))}
        </div>
        </div>
        <div className="card ml-4 overflow-x-auto p-5 divide-y-2 h-80 overflow-y-scroll">
          <div className="flex p-2"> <h1 className="text-xl font-bold mt-1 mr-2"> Ledger Types</h1><Addseriesmodal/></div>
        {series.map((led,index)=>(<div className="flex flex-rows items-center" key={`${led.Series_Name}+${index}`}>{led.Series_Name}</div>))}
        </div>
      </div>
    </Layout>
  )
}