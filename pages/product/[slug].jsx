import React, { useContext, useState } from 'react';
import Layout from '../../components/Layout/Layout';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { Store } from '../../utils/Store';
import axios from 'axios';
import { toast } from 'react-toastify';
import KeyboardArrowLeftIcon from '@mui/icons-material/KeyboardArrowLeft';
import { Button } from '@mui/material';
import { useMemo } from 'react';
import { initFirebase } from '../../firebase/firebase.App';
import { getAuth } from 'firebase/auth';
import { useAuthState } from 'react-firebase-hooks/auth';
import { db } from '../../firebase/firebase.App';
import {collection, onSnapshot, query as fireQuery, where } from "@firebase/firestore"
import { useEffect } from 'react';

export default function ProductScreen() {
  const { push } = useRouter();
  const { state, dispatch } = useContext(Store);

  const[product, setProduct] = useState()
  const[products, setProducts] = useState([])
  const {query} = useRouter()
  const slug = query.slug
  const auth = getAuth();
  const [user] = useAuthState(auth);
  const[userdet, setUserdet]= useState({})

  useEffect(() => {
    const collectionRef = collection(db, "Product")
    const q = fireQuery(collectionRef, where("slug","==",String(slug)))

    const unsubscribe = onSnapshot(q,(querySnapshot)=>{
      setProducts(querySnapshot.docs.map(doc=>({...doc.data(),id: doc.id})))
      setProduct(products[0])
    })
    return unsubscribe;
  }, [])

  async function getUser(){
    try{
      var phone_number = user.phoneNumber
      var ph = phone_number.slice(1)
      const result = await axios.post('/api/getUserDetails',{mobile_number: ph})
      setUserdet(result.data)
    }catch(error){
      console.log(error)
    }
  }

  useMemo(() => getUser(), [user])

  const [pQty, setProductQty] = useState(0);
  if (!product) {
    return <div classNameName='items-center'> Product Not Found </div>;
  }
  const handleAddToCart = async () => {
    const existItem = state.cart.cartItems.find((x) => x.slug === product.slug);
    const quantity = existItem ? existItem.quantity + pQty : pQty;
    const { data } = await axios.get(`/api/products/${product._id}`);

    if (data.countInStock < quantity) {
      return toast.error('Product out of stock');
    }
    dispatch({ type: 'CART_ADD_ITEM', payload: { ...product, quantity } });
    toast.success('Product added to the cart');
  };

  return (
    <Layout title={product.name}>
      <div className='py-4'>
        <Button
          sx={{
            textTransform: 'none',
            color: 'black',
          }}
        >
          <KeyboardArrowLeftIcon
            sx={{
              color: 'black',
            }}
          />
          <Link className='font-semibold text-lg' href='/'>
            Back
          </Link>
        </Button>
      </div>

      <section className='w-full text-gray-600 body-font overflow-hidden'>
        <div className='container py-24 mx-auto px-4'>
          <div className='mx-auto flex flex-wrap'>
            <img
              alt='ecommerce'
              className='lg:w-1/2 w-full lg:h-auto object-cover object-center rounded'
              src={product.image}
              style={{ objectFit: 'contain' }}
            />

            {/* Product Details */}
            <div className='lg:w-1/2 w-full lg:pl-10 lg:py-6 mt-6 lg:mt-0 flex flex-col'>
              <h2 className='text-sm title-font text-gray-500 tracking-widest'>
                Random Brand {product.brand}
              </h2>
              <h1 className='text-gray-900 text-3xl title-font font-medium mb-1'>
                {product.name}
              </h1>
              <div className='flex mb-4'></div>
              <p className='leading-relaxed'> {product.description}</p>

              <div className='flex flex-col'>
                <span className='title-font font-medium text-2xl text-gray-900'>
                  ₹{product.price}
                </span>
                <span className='mt-5'>
                  <label className='font-bold text-xl'>Quantity</label>
                  <select
                    className='mx-4'
                    value={pQty}
                    onChange={(e) => setProductQty(e.target.value)}
                  >
                    {[...Array(product.countInStock).keys()].map((x) => (
                      <option key={x + 1} value={(x + 1) * product.batchSize}>
                        {(x + 1) * product.batchSize}
                      </option>
                    ))}
                  </select>
                </span>
                <div className='flex flex-row mt-5'>
                  <button
                    className='w-40 p-2 mr-3 flex justify-center text-red-700 bg-[#F6DE8D] border-0focus:outline-none hover:bg-amber-400 active:bg-amber-500 rounded'
                    onClick={handleAddToCart}
                  >
                    Add to Cart
                  </button>
                  {userdet.role=="Admin" && <button
                    className='w-40 p-2 flex justify-center  text-red-700 bg-[#F6DE8D] border-0 focus:outline-none hover:bg-amber-400 active:bg-amber-500 rounded'
                    onClick={() => push(`/editproduct/${product.slug}`)}
                  >
                    Edit
                  </button>}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
}