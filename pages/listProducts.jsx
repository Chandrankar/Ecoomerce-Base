import React from 'react';
import Layout from '../components/Layout/Layout';
import SidebarDashboard from '../components/Sidebar/sidebarDashboard';
import { DataGrid } from '@mui/x-data-grid';
import db from '../utils/db';
import EditIcon from '@mui/icons-material/Edit';
import { useRouter } from 'next/router';
import Product from '../models/Product';

const ListProducts = (props) => {
  const { push } = useRouter();

  const columns = [
    { field: 'name', headerName: 'Name', width: 100 },
    { field: 'category', headerName: 'Category', width: 100 },
    { field: 'price', headerName: 'Price', width: 100 },
    { field: 'batchSize', headerName: 'Size of Batch', width: 100 },
    {
      field: 'countInStock',
      headerName: 'Remaining Stock',
      width: 180,
    },
    { field: 'description', headerName: 'Product Details', width: 200 },
    { field: 'listing', headerName: 'Is Listed', width: 80 },
    {
      field: 'edit',
      headername: 'Edit Product',

      width: 100,
      renderCell: ({ row: { slug } }) => {
        return (
          <div>
            <button onClick={() => push(`/editproduct/${slug}`)}>
              <EditIcon />
            </button>
          </div>
        );
      },
    },
  ];

  return (
    <div className='flex'>
      <SidebarDashboard />
      <div className='mx-4 my-4 w-full overflow-x-scroll'>
        <DataGrid
          columns={columns}
          rows={props.products}
          getRowId={(pro) => pro._id}
        />
      </div>
    </div>
  );
};

export async function getServerSideProps() {
  await db.connect();
  const products = await Product.find().limit().lean();
  await db.disconnect();
  return {
    props: {
      products: products.map(db.convertDocToObj),
    },
  };
}

export default ListProducts;
