import db from '../../utils/db';
import User from '../../models/User';
import { DataGrid } from '@mui/x-data-grid';
import Addusermodal from '../../components/modals/addusermodal';
import Layout from '../../components/Layout/Layout';
import SidebarDashboard from '../../components/Sidebar/sidebarDashboard';
import { useRouter } from 'next/router';
import { initFirebase } from '../../firebase/firebase.App';
import { getAuth } from 'firebase/auth';
import { useAuthState } from 'react-firebase-hooks/auth';
import axios from 'axios';
import { useState } from 'react';
import { useMemo } from 'react';
import LockOpenOutlinedIcon from "@mui/icons-material/LockOpenOutlined";
import { toast } from 'react-toastify';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';

export default function OrganizationScreen(props) {
  //console.log(props)
  const router = useRouter();
  

  const auth = getAuth();
  const [user] = useAuthState(auth);
  const[userdet, setUserdet]= useState({})

  async function getUser(){
    if(!user){return}
    try{
      var phone_number = user.phoneNumber
      var ph = phone_number.slice(1)
      const result = await axios.post('/api/getUserDetails',{mobile_number: ph})
      setUserdet(result.data)
    }catch(error){
      console.log(error)
    }
  }

  useMemo(() => getUser(), [user])

  const lockUser =async(mobile)=>{
    try{var result = await axios.post('/api/lockuser',{ph:mobile})
    toast.success("User is Blocked")
  }
    catch(error){
      toast.error("Cannot Block User")
    }
  }

  const unlockUser =async(mobile)=>{
    try{var result = await axios.post('/api/unlockuser',{ph:mobile})
    toast.success("User is unblocked")
  }
    catch(error){
      toast.error("User cannot be unblocked")
    }
  }

  const columns = [
    { field: '_id', headerName: 'Organization Id', width: 300 },
    { field: 'name', headerName: 'Name', width: 200 },
    { field: 'mobile_number', headerName: 'Number', width: 150 },
    { field: 'role', headerName: 'Role', width: 150 },
    {
      field: 'Files',
      headername: 'Show Details',
      width: 100,
      renderCell: ({ row: { _id } }) => {
        return (
          <div>
            <button onClick={() => router.push(`/user/${_id}`)}>
              Show Files
            </button>
          </div>
        );
      },
    },
    {
      field: 'Specials',
      headername: 'Show Details',
      width: 100,
      renderCell: ({ row: { _id } }) => {
        return (
          <div>
            <button onClick={() => router.push(`/specials/${_id}`)}>
              Show Specials
            </button>
          </div>
        );
      },
    },
    {
      field: 'Edit',
      headername: 'Show Details',
      width: 100,
      renderCell: ({ row: { _id } }) => {
        return (
          <div>
            <button onClick={() => router.push(`/edituser/${_id}`)}>
              Edit Details
            </button>
          </div>
        );
      },
    },
    {
      field: 'Access',
      headername: 'Show Details',
      width: 100,
      renderCell: ({ row: { mobile_number, allowed_sign_in } }) => {
        return (
          <div>
           {allowed_sign_in && <button onClick={() =>lockUser(mobile_number)}>
              <LockOutlinedIcon/>
            </button>}
            {!allowed_sign_in && <button onClick={() =>unlockUser(mobile_number)}>
              <LockOpenOutlinedIcon/>
            </button>}
          </div>
        );
      },
    },
  ];
  return (
    <div className='flex'>
      <SidebarDashboard />
      <div className='mx-4 my-4 w-full overflow-x-scroll'>
        <>
          {userdet.role!="User" && <Addusermodal org_id={router.query.id} />}
          <DataGrid
            columns={columns}
            rows={props.users}
            getRowId={(usr) => usr._id}
          />
        </>
      </div>
    </div>
  );
}

export async function getServerSideProps(context) {
  const { params } = context;
  const { id } = params;
  console.log("slug is here", params)
  await db.connect();
  const users = await User.find({ organization: id }).lean();
  await db.disconnect();
  return {
    props: {
      users: users.map(db.convertDocToObj),
    },
  };
}
