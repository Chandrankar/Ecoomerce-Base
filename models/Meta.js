import mongoose from 'mongoose';

const metaSchema = new mongoose.Schema(
  {
    role: [{role_m:{ type: String }}],
    Ledger_Type:[{Ledger_T:{type: String}}],
    Broker:[{name:{type: String}}],
    Market:[{Market_Name:{type: String}}],
    Series: [{Series_Name:{type: String}}],
    Transport_Vendor:[{name:{type:String}}],
    Party_Grade:[{grade:{type: String}}],
}
);

const Meta = mongoose.models.Meta || mongoose.model('Meta', metaSchema);
export default Meta;