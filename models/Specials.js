import mongoose from 'mongoose';

const specialsSchema = new mongoose.Schema(
  {
    userid: { type: String, required: true},
    product:
        {
            Proid:{type:String, required: true},
            name: { type: String, required: true },
            slug: { type: String, required: true, unique: true },
            category: { type: String, required: true },
            subCategory:{type: String},
            image: { type: String, required: true },
            price: { type: Number, required: true },
            batchSize: { type: Number, required: true, default: 1 },
            countInStock: { type: Number, required: true, default: 0 },
            description: { type: String, required: true },
            isPublic: { type: Boolean, default: false },
            listing :{type: Boolean, default: true}
          },
    mobile_number: {type:Number, required: true, unique:true},
  },
  {
    timestamps: true,
  }
);

const Specials = mongoose.models.Specials || mongoose.model('Specials', specialsSchema);
export default Specials;