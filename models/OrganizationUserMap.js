import mongoose from "mongoose";
import User from "./User";
import Organization from "./Organization";

const orgUserSchema = new mongoose.Schema(
  {
    organizationID: { type: String, required: true },
    userID: { type: String, required: true },
  },
  {
    timestamps: true,
  }
);

const OrganizationUserMap =
  mongoose.models.OrganizationUserMap || mongoose.model("OrganizationUserMap", orgUserSchema);
export default OrganizationUserMap;