import mongoose from 'mongoose';

const userSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    // email: { type: String, required: true, unique: true },
    // password: { type: String },
    isValid:{type: Boolean, default: false},
    organization:{type:String, required: true},
    role: { type: String, required: true, default:"user" },
    mobile_number: {type:Number, required: true, unique:true},
    allowed_sign_in: {type: Boolean, required: true, default: false},
    PAN :{type:String},
    GST: {type: String},
    Dhara: {type: Number},
    Grace_Days: {type: Number},
    Ledger_Type:{type: String},
    TDS: {type: Number},
    TCS: {type: Number},
    Broker:{type: String},
    Market:{type: String},
    Series: {type: String},
    Account: {type: String},
    Address:{
      Address_Alternate_String:{type: String, unique: true},
      Address_Line1: {type: String},
      Address_Line2:{type: String},
      City:{type:String},
      State:{type: String},
      Country:{type:String},
      Pincode: {type:String},
      Distance:{type: Number},
      Nearest_Railway: {type: String},
      Transport_Vendor:{type: String},
    },
    Alternate_Address:{
      Address_Alternate_String:{type: String, unique: true},
      Address_Line1: {type: String},
      Address_Line2:{type: String},
      City:{type:String},
      State:{type: String},
      Country:{type:String},
      Pincode: {type:String},
      Distance:{type: Number},
      Nearest_Railway: {type: String},
      Transport_Vendor:{type: String},
    },
    Alternate_Phone:{type: Number},
    Email: {type: String},
    whatsapp: {type: Number},
    Remarks: {type: String},
    Excise_Registered_Num: {type: Number},
    Sales_Incentive:{type: Number},
    WSR:{type: Number},
    Party_Grade:{type: String},
    Debit_Limit: {
      Currency:{type: String},
      Value:{type: Number}
    },
    Limit_Days:{type: Number},
    Banks:
      {
        Bank_Account_Number: {type: String},
        Bank_Name:{type: String},
        Bank_Branch:{type: String},
        IFSC_Code:{type: String}
      },
    Insurance_Policy_Number:{type: String},
    Lower_TDS_Job_Amount:{
      Currency:{type: String},
      Amount:{type: Number}
    }
  },
  {
    timestamps: true,
  }
);

const User = mongoose.models.User || mongoose.model('User', userSchema);
export default User;