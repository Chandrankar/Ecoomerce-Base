import mongoose from "mongoose";

const orgSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    metadata: { type: JSON, required: false },
  },
  {
    timestamps: true,
  }
);

const Organization =
  mongoose.models.Organization || mongoose.model("Organization", orgSchema);
export default Organization;